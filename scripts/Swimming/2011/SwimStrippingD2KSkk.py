from Configurables import Swimming
from Gaudi.Configuration import *

Swimming().Debug = False
Swimming().DataType = '2011'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = True
Swimming().StrippingStream = 'CharmCompleteEvent'
Swimming().StrippingVersion = 'Stripping17b'
Swimming().StrippingFile = 'DstarD2KShh'
Swimming().StrippingLine = 'DstarD2KShh'
Swimming().StripCands = '/Event/Phys/D2KSKKLLForDstarD2KShh'
Swimming().OffCands = '/Event/CharmCompleteEvent/Phys/D2KSKKLLForDstarD2KShh'
Swimming().MuDSTCands = ['/Event/CharmCompleteEvent/Phys/DstarD2KShhKKLLLine']
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().SelectMethod = 'random'
Swimming().OutputType = 'MDST'
Swimming().UseFileStager = False

# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)


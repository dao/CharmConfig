from Configurables import Swimming
from Gaudi.Configuration import *

Swimming().DataType = '2011'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().DDDBtag = 'head-20110914'
Swimming().CondDBtag = 'head-20110914'
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = True
Swimming().StrippingStream = 'CharmCompleteEvent'
Swimming().StrippingVersion = 'Stripping17'
Swimming().StrippingFile = 'D2hh'
Swimming().StrippingLine = 'D2hh'
Swimming().StripCands = '/Event/Phys/D2hhPromptD2KKLine'
Swimming().OffCands = '/Event/CharmCompleteEvent/Phys/D2hhPromptD2KKLine'
Swimming().MuDSTCands = ['/Event/CharmCompleteEvent/Phys/D2hhPromptDst2D2KKLine']
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().SelectMethod = 'random'
Swimming().OutputType = 'MDST'
Swimming().UseFileStager = False

# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)


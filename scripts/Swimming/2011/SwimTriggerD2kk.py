from Configurables import Swimming
from Gaudi.Configuration import *

# Hack to make a local copy of the database so it can be opened in read-write mode.
import os
from shutil import copyfile
db = Swimming().getProp('TagDatabase')
db_local = db.split('/')[-1]
copyfile(os.path.expandvars(db), db_local)
Swimming().TagDatabase = db_local

Swimming().Debug = False
Swimming().DataType = '2011'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = False
Swimming().Hlt1Triggers = ["Hlt1TrackAllL0Decision","Hlt1TrackPhotonDecision"]
Swimming().Hlt2Triggers = ["Hlt2CharmHadD02HH_D02KKDecision", "Hlt2CharmHadD02HH_D02KKWideMassDecision"]
Swimming().OffCands = "/Event/CharmCompleteEvent/Phys/D2hhPromptD2KKLine"
Swimming().MuDSTCands = ['/Event/CharmCompleteEvent/Phys/D2hhPromptDst2D2KKLine']
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().TransformName = '2011_WithBeamSpotFilter'
Swimming().SelectMethod = 'random'
Swimming().OutputType = 'DST'
Swimming().UseFileStager = False

# TT hits fix
from Configurables import TriggerTisTos
ToolSvc().addTool(TriggerTisTos,'TriggerTisTos')
ToolSvc().TriggerTisTos.TOSFracTT = 0

# Fix needed to make Moore run
from Configurables import EventClockSvc
EventClockSvc( InitialTime = 1322701200000000000 )

# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)

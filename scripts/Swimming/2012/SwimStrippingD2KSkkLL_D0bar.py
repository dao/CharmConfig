from Configurables import Swimming
from Gaudi.Configuration import *

Swimming().Debug = False
Swimming().DataType = '2012'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = True
Swimming().StrippingStream = 'CharmToBeSwum'
Swimming().StrippingVersion = 'Stripping20'
Swimming().StrippingFile = 'DstarD2KShh'
Swimming().StrippingLineGroup = 'DstarD2KShh'
Swimming().StrippingLine = 'DstarD2KShhKKLLLine'
Swimming().StripCands = '/Event/Phys/SelConjugateD2KSKKLLForDstarD2KShh'
Swimming().OffCands = '/Event/CharmToBeSwum/Phys/SelConjugateD2KSKKLLForDstarD2KShh'
Swimming().MuDSTCands = ['/Event/CharmToBeSwum/Phys/DstarD2KShhKKLLLine']
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().SelectMethod = 'random'
Swimming().OutputType = 'MDST'
Swimming().UseFileStager = False

# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)

from Gaudi.Configuration import appendPostConfigAction
from Configurables import Swimming
triggerstorun = Swimming().getProp("Hlt1Triggers") + Swimming().getProp("Hlt2Triggers")

PrescalerFiddlePrefix = 'DeterministicPrescaler/'
PrescalerFiddleSuffix = '(?!Hlt1Global)(?!Hlt2Global).*PreScaler'
PrescalerFiddle = "".join(['(?!%s)'%trigger.split('Decision')[0] for trigger in triggerstorun])
print PrescalerFiddlePrefix+PrescalerFiddle+PrescalerFiddleSuffix
fixedinclusivedstartransform = {
    PrescalerFiddlePrefix+PrescalerFiddle+PrescalerFiddleSuffix : { 'AcceptFraction' : {'.*' : '0'} },
    'DeterministicPrescaler/Hlt2ForwardPreScaler' : { 'AcceptFraction' : {'.*' : '1'}} ,
    '.*HltPV3D' : {'Code' : {"'PV3D'":"'PV3D_PreSwim'"}},
    'GaudiSequencer/Hlt2CharmHadD02HHXDst_hhX(|WideMass)FilterSequence' : {
      'Members' : { ", 'FilterDesktop/Hlt2CharmHadD02HHXDstSlowPionFilter'" : "", ", 'CombineParticles/Hlt2CharmHadD02HHXDst_hhX(|WideMass)Combine_Dstar(|_CharmHadD02HHXDst_hhX)(|WideMass)'" : ""}
      },
    'HltCopySelection<LHCb::Particle>/Hlt2CharmHadD02HHXDst_hhXDecision' : {
      'InputSelection' : { 'Combine_Dstar' : 'Hlt1TOSFilter'}
      },
    'HltCopySelection<LHCb::Particle>/Hlt2CharmHadD02HHXDst_hhXWideMassDecision' : {
      'InputSelection' : { 'WideMassCombine_Dstar' : 'Hlt1TOSFilter', '_CharmHadD02HHXDst_hhXWideMass' : '_CharmHadD02HHXDst_hhX' }
      },
    'CombineParticles/Hlt2CharmHadTwoBodyForD02HHHHHHXCombineHHX' : { 'MotherCut' : { '^.*$' :\
        "(BPVVD> 0.0 )& (BPVCORRM < 3500.0)& (BPVVDCHI2> 100.0 ) & (BPVDIRA > 0.99) & (M < 2500.0) & (VFASPF(VCHI2PDOF) < 20.0)&(INGENERATION((('pi+'==ABSID)|('K+'==ABSID))&(TRCHI2DOF < 2.25),1)) & (INGENERATION((('pi+'==ABSID)|('K+'==ABSID))&(MIPCHI2DV(PRIMARY) > 36.0),1))"}},
    'GaudiSequencer/.*' : {'Members':{"HltPV3D'":"HltPV3D', 'HltMoveVerticesForSwimming/HltMovePVs4Swimming'"}}
    }

def hackTransform():
  from Configurables import HltConfigSvc
  print HltConfigSvc()
  HltConfigSvc().ApplyTransformation = fixedinclusivedstartransform
  print HltConfigSvc()

appendPostConfigAction(hackTransform)

from Configurables import Swimming
from Gaudi.Configuration import *

d0loc = "/Event/CharmToBeSwum/Phys/D2hhPromptD2{hh}D2hhHlt2TOS"
d0barloc = "/Event/CharmToBeSwum/Phys/SelConjugateD2hhPromptD2{hh}"
dstloc = '/Event/CharmToBeSwum/Phys/D2hhPromptDst2D2{hh}Line'
wsloc = '/Event/CharmToBeSwum/Phys/D2hhPromptD0WS'
tmp = [
    (d0loc, 'PiPi', 'PiPi', d0loc), 
    (d0barloc, 'PiPi', 'PiPi', d0loc), 
    (d0loc, 'KK', 'KK', d0loc), 
    (d0barloc, 'KK', 'KK', d0loc), 
    (d0loc, 'KPi', 'RS', d0loc),
    (wsloc, 'KPi', 'WS', d0loc)
    ]
Swimming().Debug = False
Swimming().DataType = '2012'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = True
Swimming().StrippingStream = 'CharmToBeSwum'
Swimming().StrippingVersion = 'Stripping20'
Swimming().StrippingFile = 'D2hh'
Swimming().StrippingLineGroup = 'D2hh'
Swimming().OffCands   = dict( (loc.format(hh=hh), dstloc.format(hh=dsthh)) for loc, hh, dsthh, striploc in tmp )
stream = Swimming().getProp('StrippingStream') + '/'
Swimming().StripCands = dict( (loc.format(hh=hh), [striploc.format(hh=hh).replace(stream, '')]) for loc, hh, dsthh, striploc in tmp )
Swimming().MuDSTCands = [ dstloc.format(hh=hh) for hh in ['RS','WS','KK','PiPi']]
Swimming().StrippingLines = [l.split('/')[-1] for l in Swimming().getProp('MuDSTCands')]
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().SkipEventIfNoMuDSTCandsAnywhere = False
Swimming().SelectMethod = 'all'
Swimming().OutputType = 'MDST'
Swimming().UseFileStager = False
Swimming().LifetimeFitter = ['DecayTreeFitter', 'LifetimeFitter']
Swimming().DecayTreeFitterConstraints = {
    'DTFD0'  : { 'D0' : -1.0 },
    'DTF'    : { }  
    }
Swimming().StoreExtraTPs = True

# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)

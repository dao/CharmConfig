from Configurables import Swimming
from Gaudi.Configuration import *

Swimming().Debug = False
Swimming().DataType = '2012'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = False
Swimming().Hlt1Triggers = ["Hlt1TrackAllL0Decision","Hlt1TrackPhotonDecision"]
Swimming().Hlt2Triggers = ["Hlt2CharmHadD02HHXDst_hhXDecision","Hlt2CharmHadD02HHXDst_hhXWideMassDecision"]
Swimming().OffCands = "/Event/CharmToBeSwum/Phys/D2KSKKDDForDstarD2KShh"
Swimming().MuDSTCands = ['/Event/CharmToBeSwum/Phys/DstarD2KShhKKDDLine']
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().TransformName = '2012_WithBeamSpotFilter_NoRecoLines_ForInclusiveDstar'
Swimming().SelectMethod = 'random'
Swimming().OutputType = 'DST'
Swimming().UseFileStager = False

# TT hits fix
from Configurables import TriggerTisTos
ToolSvc().addTool(TriggerTisTos,'TriggerTisTos')
ToolSvc().TriggerTisTos.TOSFracTT = 0


# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)

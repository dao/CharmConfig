from Configurables import Swimming
from Gaudi.Configuration import *

Swimming().DataType = '2012'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = True
Swimming().StrippingStream = 'CharmToBeSwum'
Swimming().StrippingVersion = 'Stripping20'
Swimming().StrippingFile = 'D2hh'
Swimming().StrippingLineGroup = 'D2hh'
Swimming().StrippingLine  = 'D2hhPromptDst2D2KKLine'
Swimming().StripCands = "/Event/Phys/D2hhPromptD2KKD2hhHlt2TOS"
Swimming().OffCands = "/Event/CharmToBeSwum/Phys/D2hhPromptD2KKD2hhHlt2TOS"
Swimming().MuDSTCands = ['/Event/CharmToBeSwum/Phys/D2hhPromptDst2D2KKLine']
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().SelectMethod = 'random'
Swimming().OutputType = 'MDST'
Swimming().UseFileStager = False

# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)


from Configurables import Swimming
from Gaudi.Configuration import *

Swimming().Debug = False
Swimming().DataType = '2012'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = True
Swimming().StrippingStream = 'CharmToBeSwum'
Swimming().StrippingVersion = 'Stripping20'
Swimming().StrippingFile = 'DstarD2KShh'
Swimming().StrippingLineGroup = 'DstarD2KShh'
Swimming().StrippingLine = 'DstarD2KShhKKDDLine'
Swimming().StripCands = '/Event/Phys/SelConjugateD2KSKKDDForDstarD2KShh'
Swimming().OffCands = '/Event/CharmToBeSwum/Phys/SelConjugateD2KSKKDDForDstarD2KShh'
Swimming().MuDSTCands = ['/Event/CharmToBeSwum/Phys/DstarD2KShhKKDDLine']
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().SelectMethod = 'random'
Swimming().OutputType = 'MDST'
Swimming().UseFileStager = False

# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)

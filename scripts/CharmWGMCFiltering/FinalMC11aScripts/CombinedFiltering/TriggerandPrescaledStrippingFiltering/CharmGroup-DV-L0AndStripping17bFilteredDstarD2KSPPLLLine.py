"""
Options for building Stripping17b with the KShhLL stripping line in the MC AllStreams stream
"""

from Gaudi.Configuration import *

#---------- Include trigger filtering

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (

# L0 code
L0DU_Code = "L0_DECISION_PHYSICS",

# HLT1 code 
#HLT_Code = "HLT_PASS_RE('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision')"

)

#------------------------------------


# Rest is stripping
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object - bring in some necessary modules
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams

# Build stripping in a slightly different way as S17b is not in the Archive for DV v29r2p* stack required by the swimming
strippingVersion = 'stripping17b'
strippingLine    = 'DstarD2KShh'

from StrippingSettings.Utils import lineBuilderConfiguration
config = lineBuilderConfiguration( strippingVersion, strippingLine )
import StrippingSelections
lineconf = getattr(__import__('StrippingSelections.Stripping' + strippingLine,
                              globals(),locals(),
                              [config["BUILDERTYPE"]],-1),config["BUILDERTYPE"])
linedict = config["CONFIG"]
line = lineconf(strippingLine, linedict).lines()


#Select the streams you are interested in - in this case just the charm complete event stream
myStream = StrippingStream("DstarD2KSPPLL.StripTrig")
myStream.appendLines( line )

sc = StrippingConf( Streams = [ myStream ],
                    MaxCandidates = 2000 )        # Max candidates

myStream.sequence().IgnoreFilterPassed = False # we wish to run the stripping in rejection mode


# Import the MDST and DST writers - these are altered as we wish to use an older DV version to be compatible with the KShh dataset
from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = '',
                          SelectionSequences = sc.activeStreams()
                          )


#------------------------------------ 
# Testing
#from Configurables import EventNodeKiller
#eventNodeKiller = EventNodeKiller('Stripkiller')
#eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#-------------------------------------


#------------------------- DaVinci Configuration
from Configurables import DaVinci
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters') #Prefilter on the trigger decisions
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                       # Number of events
DaVinci().HistogramFile = "DVHistos.root"

#------------------
# Testing
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#-----------------

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
# Hardcoded tags need to be removed
#DaVinci().DDDBtag    = 'MC11-20111102'
#DaVinci().CondDBtag  = "sim-20111111-vc-md100"

##----------------
#DaVinci().UseTrigRawEvent=True
##--------------

#from GaudiConf import IOHelper
#IOHelper().inputFiles(['/afs/cern.ch/work/n/ntorr/signalmix_dst/00021030_00000005_1.allstreams.dst'], clear = True)

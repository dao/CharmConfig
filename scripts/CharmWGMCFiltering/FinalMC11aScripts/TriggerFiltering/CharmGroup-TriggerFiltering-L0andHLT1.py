# Import some useful modules
from Gaudi.Configuration import *

#----------Include trigger filtering---------------------------

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (

# L0 code
L0DU_Code = "L0_DECISION_PHYSICS",

# HLT1 code 
HLT_Code = "HLT_PASS_RE('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision')"

    )

#----------------------DST writing - use InputCopyStream----------------------------------------------------------

dstWriterName="DstWriter"
DstWriter = InputCopyStream(dstWriterName)
DstWriter.Output = "DATAFILE=\'PFN:test.dst\' TYP=\'POOL_ROOTTREE\' OPT=\'REC\' "
DstWriter_GaSeq = GaudiSequencer(dstWriterName+"_GaSeq") # create GaudiSequencer with the DstWriter
DstWriter_GaSeq.Members =[DstWriter]

#------------------------------------
#
# DaVinci Configuration
#

from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                       # Number of events
#DaVinci().HistogramFile = "DVHistos.root"
#DaVinci().TupleFile = "TupleFile.root"
DaVinci().appendToMainSequence( [DstWriter_GaSeq] )
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

## ## Test data
## EventSelector().Input   =[

## # Written out DST

## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000001_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000002_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000003_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000004_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000005_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'"

##                          ]



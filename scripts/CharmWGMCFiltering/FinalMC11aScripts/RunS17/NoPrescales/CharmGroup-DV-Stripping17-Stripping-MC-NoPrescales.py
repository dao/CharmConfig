"""
Options for building Stripping17 with strict ordering
of streams such that the micro-DSTs come last.
"""

#---------- Include trigger filtering

# Trigger filtering goes here
# Waiting for Gloria's pre-production sample and new grid rights 08/12/11 

#------------------------------------

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object - bring in some necessary modules
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)          # Goes away and gets the relevant stripping cut dictionaries from Phys/StrippingSettings
#get the line builders from the archive
archive = strippingArchive(stripping)       # Goes away and gets the relevant line builders from Phys/StrippingSelections

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []        

#Select the streams you are interested in - in this case just the charm complete event and MDST streams 

_charm_complete = quickBuild('CharmCompleteEvent')
_charm_micro    = quickBuild('Charm')

streams.append( _charm_complete )
streams.append( _charm_micro )

#
#----------------------------------- turn off all pre-scalings 
#
for stream in streams:                  ## Prescaling 
    for line in stream.lines:
        line._prescale = 1.0 

#---------------------------------------------------------------

#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("Dst_D0pi.Strip.Trig")

for stream in streams:                       # stream = "member" of streams
    AllStreams.appendLines(stream.lines)          # add all the lines associated with each of the streams

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000 )        # Max candidates

AllStreams.sequence().IgnoreFilterPassed = False # we wish to run the stripping in rejection mode


# Import the MDST and DST writers
from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )

#
#------------------------- DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                       # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().DDDBtag = 'MC11-20111102'
DaVinci().CondDBtag  = "sim-20111111-vc-mu100"
DaVinci().UseTrigRawEvent=True

## ##Test data
## EventSelector().Input   =[

# D2KS0Pi signal MC 
#"DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/MC11a/D2KS0Pi/00013764_00000001_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'"

## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000001_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000002_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000003_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000004_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
## "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/CharmMCFiltering/S17/D02Kpi/00013183_00000005_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",

##                          ]



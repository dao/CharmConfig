"""
Turbo Filtering script for Monte Carlo for Hlt2CharmHadXic0ToPpKmKmPip_LTUNBTurbo
@author Dong Ao
@date   2018-03-14
"""

from Configurables import GaudiSequencer

linename = 'Hlt2CharmHadXic0ToPpKmKmPip_LTUNBTurbo'
#trigger filter
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence
line =AutomaticData('/Event/Turbo/'+linename+'/Particles')
selseq = SelectionSequence('SEQ',line)

#
# Configuration of SelDSTWriter
#
enablePacking =True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )
SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }

dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='TurboFiltered',
                         SelectionSequences= [ selseq ]
                         )
#
# DumpFSR test
#
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

						 
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

"""
from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00062030/0000/00062030_00000001_7.AllStreams.dst',
'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00062030/0000/00062030_00000002_7.AllStreams.dst',
'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00062030/0000/00062030_00000003_7.AllStreams.dst',
'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00062030/0000/00062030_00000004_7.AllStreams.dst'

], clear=True)
"""



"""

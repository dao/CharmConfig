OPTIONS_DIR="\$CHARMCONFIGROOT/options/WGProductions/D2V0H"

run_test \
    "output/D2KS0H_D2PhiPi-2017/" \
    "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMCHARGED.MDST" \
    "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo04/94000000/CHARMCHARGED.MDST" \
    "" "" "133172"

run_test \
    "output/Ds2PhiPi-2017/" \
    "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMSPEC.MDST" \
    "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo04/94000000/CHARMSPEC.MDST" \
    "" "" "133173"

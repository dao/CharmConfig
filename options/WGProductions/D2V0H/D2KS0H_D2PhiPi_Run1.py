import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolDecay
from Configurables import TupleToolGeometry, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolPid, TupleToolEventInfo, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTrigger
from Configurables import TupleToolTISTOS, L0TriggerTisTos, TriggerTisTos
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool
from Configurables import TupleToolDecayTreeFitter

TupleToolList = [
    "TupleToolGeometry",
    "TupleToolKinematic",
   # "TupleToolPropertime",
    "TupleToolPrimaries",
    "TupleToolPid",
    "TupleToolEventInfo",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    "TupleToolTISTOS",
    "TupleToolTrigger"
    ]

TriggerList = [
    'L0HadronDecision',
    'L0MuonDecision',
    'Hlt1TrackAllL0Decision',
    'Hlt2CharmHadD2KS0H_D2KS0PiDecision',
    'Hlt2CharmHadD2KS0H_D2KS0DDPiDecision',
    'Hlt2CharmHadD2KS0H_D2KS0KDecision',
    'Hlt2CharmHadD2KS0H_D2KS0DDKDecision',
    'Hlt2CharmHadD2HHHDecision'
    ]

LoKi_particle=LoKi__Hybrid__TupleTool("LoKi_particle")
LoKi_particle.Variables =  {
    "ETA" : "ETA",
    "PHI" : "PHI"
    }
LoKi_Dplus2KS0h=LoKi__Hybrid__TupleTool("LoKi_Dplus2KS0h")
LoKi_Dplus2KS0h.Variables =  {
    'DOCACHI2_KS0_hplus' : 'DOCACHI2(1,2)',
    'DOCA_KS0_hplus' : 'DOCA(1,2)',
    }
LoKi_Dplus2PhiPi=LoKi__Hybrid__TupleTool("LoKi_Dplus2PhiPi")
LoKi_Dplus2PhiPi.Variables =  {
    'DOCACHI2_Kplus_Kminus' : 'DOCACHI2(1,2)',
    'DOCA_Kplus_Kminus' : 'DOCA(1,2)',
    'DOCACHI2_Kplus_piplus' : 'DOCACHI2(1,3)',
    'DOCA_Kplus_plus' : 'DOCA(1,3)',
    'DOCACHI2_Kminus_piplus' : 'DOCACHI2(2,3)',
    'DOCA_Kminus_piplus' : 'DOCA(2,3)'
 }
LoKi_KS0=LoKi__Hybrid__TupleTool("LoKi_KS0")
LoKi_KS0.Variables = {
   'DOCACHI2_piplus_piminus' : 'DOCACHI2(1,2)',
   'DOCA_piplus_piminus' : 'DOCA(1,2)',
   }

################# D -> KS0 H #####################################

# D -> KS0 Pi

tuple_KsPiLL = DecayTreeTuple( 'Dp2KS0pipLL'  )
tuple_KsPiLL.Inputs = ['Phys/D2KS0HPionLine/Particles' ]

tuple_KsPiDD = DecayTreeTuple( 'Dp2KS0pipDD'  )
tuple_KsPiDD.Inputs = ['Phys/D2KS0HPionLineDD/Particles']

for tuple in [tuple_KsPiLL,tuple_KsPiDD]:
    tuple.Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC"
    tuple.Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
        }

# D -> KS0 K

tuple_KsKLL = DecayTreeTuple( 'Dp2KS0KpLL'  )
tuple_KsKLL.Inputs = ['Phys/D2KS0HKaonLine/Particles']

tuple_KsKDD = DecayTreeTuple( 'Dp2KS0KpDD'  )
tuple_KsKDD.Inputs = ['Phys/D2KS0HKaonLineDD/Particles']

for tuple in [tuple_KsKLL,tuple_KsKDD]:

    tuple.Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC"
    tuple.Branches = {
        "Dplus"       : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
        "KS0"      : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
        "hplus"       : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC"
        }

# D -> KS0 H

for tuple in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD]:

    tuple.addTool(TupleToolDecay, name="Dplus")
    tuple.addTool(TupleToolDecay, name="KS0")
    tuple.addTool(TupleToolDecay, name="piplus")
    tuple.addTool(TupleToolDecay, name="piminus")
    tuple.addTool(TupleToolDecay, name="hplus")
 #LoKi VARIABLES
    for particle in [tuple.Dplus,tuple.KS0,tuple.piplus,tuple.piminus,tuple.hplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    tuple.Dplus.addTool(LoKi_Dplus2KS0h)
    tuple.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2KS0h"]
    tuple.KS0.addTool(LoKi_KS0)
    tuple.KS0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_KS0"]

#DECAY TREE FITTER
    tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTF')
    # tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMass')
    tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
#constrain PV and mass
    tuple.Dplus.DTF.constrainToOriginVertex = True
    tuple.Dplus.DTF.Verbose = True
    tuple.Dplus.DTF.daughtersToConstrain = ['KS0']
    tuple.Dplus.DTF.UpdateDaughters = True
#constrain only PV
    tuple.Dplus.DTFonlyPV.constrainToOriginVertex = True
    tuple.Dplus.DTFonlyPV.Verbose = True
    tuple.Dplus.DTFonlyPV.UpdateDaughters = True
#constrain only mass
    # tuple.Dplus.DTFonlyMass.constrainToOriginVertex = False
    # tuple.Dplus.DTFonlyMass.Verbose = True
    # tuple.Dplus.DTFonlyMass.daughtersToConstrain = ['KS0']
    # tuple.Dplus.DTFonlyMass.UpdateDaughters = True

################# D -> Phi Pi #####################################

tuple_PhiPi = DecayTreeTuple('Dp2KmKppip')
tuple_PhiPi.Inputs = ['Phys/D2hhh_KKPLine/Particles']

for tuple in [tuple_PhiPi]:
    tuple.Decay = "[D+ -> ^K+  ^K- ^pi+]CC"
    tuple.Branches = {
        "Dplus"   : "[D+ -> K+  K- pi+]CC",
        "Kplus"   : "[D+ -> ^K+  K- pi+]CC",
        "Kminus"  : "[D+ -> K+  ^K- pi+]CC",
        "piplus"  : "[D+ -> K+  K- ^pi+]CC"
        }
    tuple.addTool(TupleToolDecay, name="Dplus")
    tuple.addTool(TupleToolDecay, name="Kplus")
    tuple.addTool(TupleToolDecay, name="Kminus")
    tuple.addTool(TupleToolDecay, name="piplus")

#LoKi VARIABLES
    for particle in [tuple.Dplus,tuple.Kplus,tuple.Kminus,tuple.piplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    tuple.Dplus.addTool(LoKi_Dplus2PhiPi)
    tuple.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2PhiPi"]

#DECAY TREE FITTER
    tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
#constrain only PV
    tuple.Dplus.DTFonlyPV.constrainToOriginVertex = True
    tuple.Dplus.DTFonlyPV.Verbose = True
    tuple.Dplus.DTFonlyPV.UpdateDaughters = True


################# D -> KS0 H, D -> Phi Pi #####################################

tttt = TupleToolTISTOS()
tttt.addTool(L0TriggerTisTos())
tttt.addTool(TriggerTisTos())

for tuple in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD,tuple_PhiPi]:

    tuple.TupleName = "ntp"

    tuple.ToolList =  TupleToolList

    tuple.addTool(TupleToolTrigger())
    tuple.TupleToolTrigger.VerboseL0 = True
    tuple.TupleToolTrigger.VerboseHlt1 = True
    tuple.TupleToolTrigger.VerboseHlt2 = True
    tuple.TupleToolTrigger.TriggerList = TriggerList

    tuple.addTool(tttt)
    tuple.TupleToolTISTOS.Verbose = True
    tuple.TupleToolTISTOS.VerboseL0 = True
    tuple.TupleToolTISTOS.VerboseHlt1 = True
    tuple.TupleToolTISTOS.VerboseHlt2 = True
    tuple.TupleToolTISTOS.TriggerList = TriggerList

############ DaVinci configuration ###############################
lineKS0PiLL = "Hlt2CharmHadD2KS0H_D2KS0Pi"
lineKS0PiDD = "Hlt2CharmHadD2KS0H_D2KS0DDPi"
lineKS0KLL = "Hlt2CharmHadD2KS0H_D2KS0K"
lineKS0KDD = "Hlt2CharmHadD2KS0H_D2KS0DDK"
lineD2PhiPi = "Hlt2CharmHadD2HHH"

from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters

fltrs = LoKi_Filters( Code = "HLT_PASS_RE('"+lineKS0PiLL+"Decision') | HLT_PASS_RE('"+lineKS0PiDD+"Decision') | HLT_PASS_RE('"+lineKS0KLL+"Decision') | HLT_PASS_RE('"+lineKS0KDD+"Decision') | HLT_PASS_RE('"+lineD2PhiPi+"Decision')")


DaVinci().Simulation   = False
DaVinci().Lumi = True
DaVinci().RootInTES = "/Event/Charm/"
DaVinci().appendToMainSequence([tuple_KsPiLL])
DaVinci().appendToMainSequence([tuple_KsPiDD])
DaVinci().appendToMainSequence([tuple_KsKLL])
DaVinci().appendToMainSequence([tuple_KsKDD])
DaVinci().appendToMainSequence([tuple_PhiPi])
# DaVinci().EvtMax = 300 #
# DaVinci().PrintFreq = 50 #
# DaVinci().DataType = "2011" #
DaVinci().TupleFile = "tuple_D2KS0H_D2PhiPi.root"
DaVinci().InputType='MDST'

################# READ FROM LOCAL FILE ###########################
# from GaudiConf import IOHelper
# IOHelper().inputFiles([
#          '/eos/lhcb/user/f/fferrari/forSerena/00022727_00027438_1.charm.mdst' #stripping20r1 (2011)
#         ], clear = True)


# Helper file to define data type for davinci

year      = '2011'
fileType  = 'MDST'
rootintes = "/Event/Charm"

from Configurables import DaVinci, CondDB
dv = DaVinci (  DataType                  = year           ,
                InputType                 = fileType       ,
                RootInTES                 = rootintes      ,
		Simulation		  = False,
                Lumi                      = True
	             )
db = CondDB  ( LatestGlobalTagByDataType = year )

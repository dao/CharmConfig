#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

./create_WG_production.py --create --submit --print \
    --name='WG Charm RareCharm 2011 CHARM DVv36r7p7 S21r1' \
    --author='cburr' --step='133320' --step='132774' --dq-flag='OK' \
    --bk-query='/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/CHARM.MDST'


./create_WG_production.py --create --submit --print \
    --name='WG Charm RareCharm 2011 CHARM DVv36r7p7 S21r1' \
    --author='cburr' --step='133320' --step='132774' --dq-flag='OK' \
    --bk-query='/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/CHARM.MDST'

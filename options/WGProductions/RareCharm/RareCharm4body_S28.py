# Authors: O. Kochebina and B. Viaud
# Date   : 18/11/2011
# changed: D. Mitzel 24/2/2017, A Contu 22/6/2017 
# adapted to stripping28 , Dvinci v42r4
# added variables  

############ A few preliminary informations ############


## ATTENTION!!! New variables are added not to everytuple!!!!! CHECK!!


from Gaudi.Configuration import *
MessageSvc().Format = "% F%80W%S%7W%R%T %0W%M"
from Configurables import DaVinci, PrintDecayTree
from Configurables import LoKi__HDRFilter as StripFilter
# get classes to build the SelectionSequence
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, MultiSelectionSequence

# Filter the Candidate
from Configurables import FilterDesktop

#DecayTreeTuple
from Configurables import DecayTreeTuple
from Configurables import TupleToolTrigger
from Configurables import TupleToolGeometry
from Configurables import TupleToolKinematic
from Configurables import TupleToolPropertime
from Configurables import TupleToolPrimaries
from Configurables import TupleToolEventInfo
from Configurables import TupleToolPid
from Configurables import TupleToolMuonPid
#from Configurables import TupleToolRICHPid
from Configurables import TupleToolTrackInfo
from Configurables import TupleToolDecay
from Configurables import TupleToolP2VV
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import TupleToolTrigger
from Configurables import TupleToolTISTOS
from Configurables import TupleToolL0Calo
from Configurables import TupleToolTrackIsolation
from Configurables import TupleToolDecayTreeFitter
from Configurables import TupleToolANNPID
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['DAQ',
                         'pRec']


############ Few things are common to all ntuples  #####

# Tools in the ntuples
List_of_Tools = [
 "TupleToolEventInfo"
#,"TupleToolTrigger" ###
,"TupleToolEventInfo"
,"TupleToolGeometry"
,"TupleToolKinematic"
,"TupleToolPid"
#,"TupleToolRICHPid"
,"TupleToolMuonPid"
,"TupleToolPrimaries"
,"TupleToolPropertime"
,"TupleToolTrackInfo"
#,"TupleToolRecoStats"
,"TupleToolTISTOS"
,"TupleToolL0Calo"
,"TupleToolANNPID"
]


# List of variables that do not come by default for D->HHmumu
LoKi_All_hhmumu = LoKi__Hybrid__TupleTool("LoKi_All_hhmumu")
LoKi_All_hhmumu.Variables={
     "DiMuon_Mass" : "M34"
    ,"LV01" : "LV01"
    ,"LV02" : "LV02"
    ,"LV03" : "LV03"
    ,"LV04" : "LV04"
     ,"MAXDOCA" : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
    }


from Configurables import LoKi__Hybrid__TupleTool
LoKi_Cone =  LoKi__Hybrid__TupleTool( 'LoKi_Cone')
LoKi_Cone.Variables = {
  "CONEANGLE_D_11"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEANGLE',-1.)",
  "CONEMULT_D_11"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEMULT', -1.)",
  "CONEPTASYM_D_11"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar_11"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEANGLE',-1.)",
  "CONEMULT_Dstar_11"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEMULT',-1.)",
  "CONEPTASYM_Dstar_11" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEPTASYM',-1.)",
  "CONEANGLE_D_13"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEANGLE',-1.)",
  "CONEMULT_D_13"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEMULT', -1.)",
  "CONEPTASYM_D_13"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar_13"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEANGLE',-1.)",
  "CONEMULT_Dstar_13"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEMULT',-1.)",
  "CONEPTASYM_Dstar_13" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEPTASYM',-1.)",
  "CONEANGLE_D"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEANGLE',-1.)",
  "CONEMULT_D"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEMULT', -1.)",
  "CONEPTASYM_D"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEANGLE',-1.)",
  "CONEMULT_Dstar"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEMULT',-1.)",
  "CONEPTASYM_Dstar" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEPTASYM',-1.)",
  "CONEANGLE_D_17"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEANGLE',-1.)",
  "CONEMULT_D_17"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEMULT', -1.)",
  "CONEPTASYM_D_17"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar_17"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEANGLE',-1.)",
  "CONEMULT_Dstar_17"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEMULT',-1.)",
  "CONEPTASYM_Dstar_17" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEPTASYM',-1.)"
}

from Configurables import LoKi__Hybrid__TupleTool
LoKi_Cone_had =  LoKi__Hybrid__TupleTool( 'LoKi_Cone_had')
LoKi_Cone_had.Variables = {
  "CONEANGLE_D_11"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEANGLE',-1.)",
  "CONEMULT_D_11"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEMULT', -1.)",
  "CONEPTASYM_D_11"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar_11"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEANGLE',-1.)",
  "CONEMULT_Dstar_11"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEMULT',-1.)",
  "CONEPTASYM_Dstar_11" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEPTASYM',-1.)",
  "CONEANGLE_D_13"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEANGLE',-1.)",
  "CONEMULT_D_13"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEMULT', -1.)",
  "CONEPTASYM_D_13"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar_13"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEANGLE',-1.)",
  "CONEMULT_Dstar_13"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEMULT',-1.)",
  "CONEPTASYM_Dstar_13" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEPTASYM',-1.)",
  "CONEANGLE_D"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEANGLE',-1.)",
  "CONEMULT_D"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEMULT', -1.)",
  "CONEPTASYM_D"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEANGLE',-1.)",
  "CONEMULT_Dstar"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEMULT',-1.)",
  "CONEPTASYM_Dstar" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEPTASYM',-1.)",
  "CONEANGLE_D_17"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEANGLE',-1.)",
  "CONEMULT_D_17"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEMULT', -1.)",
  "CONEPTASYM_D_17"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar_17"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEANGLE',-1.)",
  "CONEMULT_Dstar_17"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEMULT',-1.)",
  "CONEPTASYM_Dstar_17" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEPTASYM',-1.)"
}



# List of variables that do not come by default for D->HHmumu with tag
LoKi_All_hhmumu_Dst = LoKi__Hybrid__TupleTool("LoKi_All_hhmumu_Dst")
LoKi_All_hhmumu_Dst.Variables={
    "DTF_CHI2"            : "DTF_CHI2(True)"
    , "DTF_NDOF"          : "DTF_NDOF(True)"
    , "DTF_Dstarplus_M"   : "DTF_FUN(M, True)"
    , "DTF_Dstarplus_P"   : "DTF_FUN(P, True)"
    , "DTF_Dstarplus_PT"  : "DTF_FUN(PT, True)"
    , "DTF_Dstarplus_E"   : "DTF_FUN(E, True)"
    , "DTF_Dstarplus_PX"  : "DTF_FUN(PX, True)"
    , "DTF_Dstarplus_PY"  : "DTF_FUN(PY, True)"
    , "DTF_Dstarplus_PZ"  : "DTF_FUN(PZ, True)"
    , "DTF_D0_M"          : "DTF_FUN(CHILD(M,1), True)"
    , "DTF_D0_P"          : "DTF_FUN(CHILD(P, 1), True)"
    , "DTF_D0_PT"         : "DTF_FUN(CHILD(PT, 1), True)"
    , "DTF_D0_E"          : "DTF_FUN(CHILD(E, 1), True)"
    , "DTF_D0_PX"         : "DTF_FUN(CHILD(PX, 1), True)"
    , "DTF_D0_PY"         : "DTF_FUN(CHILD(PY, 1), True)"
    , "DTF_D0_PZ"         : "DTF_FUN(CHILD(PZ, 1), True)"
    , "DTF_D0_BPVIPCHI2"  : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 1), True)"
    #, "DTF_D0_CTAU"       : "DTF_CTAU(1,TRUE)"
    , "DTF_Pis_M"         : "DTF_FUN(CHILD(M,2), True)"
    , "DTF_Pis_P"         : "DTF_FUN(CHILD(P, 2), True)"
    , "DTF_Pis_PT"        : "DTF_FUN(CHILD(PT, 2), True)"
    , "DTF_Pis_E"         : "DTF_FUN(CHILD(E, 2), True)"
    , "DTF_Pis_PX"        : "DTF_FUN(CHILD(PX, 2), True)"
    , "DTF_Pis_PY"        : "DTF_FUN(CHILD(PY, 2), True)"
    , "DTF_Pis_PZ"        : "DTF_FUN(CHILD(PZ, 2), True)"
    , "DTF_Pis_BPVIPCHI2" : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 2), True)"
    , "DTF_h0_P"          : "DTF_FUN(CHILD(CHILD(P,1),1),True)"
    , "DTF_h0_PT"         : "DTF_FUN(CHILD(CHILD(PT,1),1),True)"
    , "DTF_h0_E"          : "DTF_FUN(CHILD(CHILD(E,1),1),True)"
    , "DTF_h0_PX"         : "DTF_FUN(CHILD(CHILD(PX,1),1),True)"
    , "DTF_h0_PY"         : "DTF_FUN(CHILD(CHILD(PY,1),1),True)"
    , "DTF_h0_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,1),1),True)"
    , "DTF_h1_P"          : "DTF_FUN(CHILD(CHILD(P,2),1),True)"
    , "DTF_h1_PT"         : "DTF_FUN(CHILD(CHILD(PT,2),1),True)"
    , "DTF_h1_E"          : "DTF_FUN(CHILD(CHILD(E,2),1),True)"
    , "DTF_h1_PX"         : "DTF_FUN(CHILD(CHILD(PX,2),1),True)"
    , "DTF_h1_PY"         : "DTF_FUN(CHILD(CHILD(PY,2),1),True)"
    , "DTF_h1_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,2),1),True)"
    , "DTF_l0_P"          : "DTF_FUN(CHILD(CHILD(P,3),1),True)"
    , "DTF_l0_PT"         : "DTF_FUN(CHILD(CHILD(PT,3),1),True)"
    , "DTF_l0_E"          : "DTF_FUN(CHILD(CHILD(E,3),1),True)"
    , "DTF_l0_PX"         : "DTF_FUN(CHILD(CHILD(PX,3),1),True)"
    , "DTF_l0_PY"         : "DTF_FUN(CHILD(CHILD(PY,3),1),True)"
    , "DTF_l0_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,3),1),True)"
    , "DTF_l1_P"          : "DTF_FUN(CHILD(CHILD(P,4),1),True)"
    , "DTF_l1_PT"         : "DTF_FUN(CHILD(CHILD(PT,4),1),True)"
    , "DTF_l1_E"          : "DTF_FUN(CHILD(CHILD(E,4),1),True)"
    , "DTF_l1_PX"         : "DTF_FUN(CHILD(CHILD(PX,4),1),True)"
    , "DTF_l1_PY"         : "DTF_FUN(CHILD(CHILD(PY,4),1),True)"
    , "DTF_l1_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,4),1),True)"
    ,"MAXDOCA" : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
    ,"deltaM": "DTF_FUN(M, True) - DTF_FUN(CHILDFUN(M,'D0'==ABSID), True)"
    }

LoKi_All_hhmumu_Dst_had = LoKi__Hybrid__TupleTool("LoKi_All_hhmumu_Dst_had")
LoKi_All_hhmumu_Dst_had.Variables={
    "DTF_CHI2"            : "DTF_CHI2(True)"
    , "DTF_NDOF"          : "DTF_NDOF(True)"
    , "DTF_Dstarplus_M"   : "DTF_FUN(M, True)"
    , "DTF_Dstarplus_P"   : "DTF_FUN(P, True)"
    , "DTF_Dstarplus_PT"  : "DTF_FUN(PT, True)"
    , "DTF_Dstarplus_E"   : "DTF_FUN(E, True)"
    , "DTF_Dstarplus_PX"  : "DTF_FUN(PX, True)"
    , "DTF_Dstarplus_PY"  : "DTF_FUN(PY, True)"
    , "DTF_Dstarplus_PZ"  : "DTF_FUN(PZ, True)"
    , "DTF_D0_M"          : "DTF_FUN(CHILD(M,1), True)"
    , "DTF_D0_P"          : "DTF_FUN(CHILD(P, 1), True)"
    , "DTF_D0_PT"         : "DTF_FUN(CHILD(PT, 1), True)"
    , "DTF_D0_E"          : "DTF_FUN(CHILD(E, 1), True)"
    , "DTF_D0_PX"         : "DTF_FUN(CHILD(PX, 1), True)"
    , "DTF_D0_PY"         : "DTF_FUN(CHILD(PY, 1), True)"
    , "DTF_D0_PZ"         : "DTF_FUN(CHILD(PZ, 1), True)"
    , "DTF_D0_BPVIPCHI2"  : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 1), True)"
    #, "DTF_D0_CTAU"       : "DTF_CTAU(1,TRUE)"
    , "DTF_Pis_M"         : "DTF_FUN(CHILD(M,2), True)"
    , "DTF_Pis_P"         : "DTF_FUN(CHILD(P, 2), True)"
    , "DTF_Pis_PT"        : "DTF_FUN(CHILD(PT, 2), True)"
    , "DTF_Pis_E"         : "DTF_FUN(CHILD(E, 2), True)"
    , "DTF_Pis_PX"        : "DTF_FUN(CHILD(PX, 2), True)"
    , "DTF_Pis_PY"        : "DTF_FUN(CHILD(PY, 2), True)"
    , "DTF_Pis_PZ"        : "DTF_FUN(CHILD(PZ, 2), True)"
    , "DTF_Pis_BPVIPCHI2" : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 2), True)"
    , "DTF_h0_P"          : "DTF_FUN(CHILD(CHILD(P,1),1),True)"
    , "DTF_h0_PT"         : "DTF_FUN(CHILD(CHILD(PT,1),1),True)"
    , "DTF_h0_E"          : "DTF_FUN(CHILD(CHILD(E,1),1),True)"
    , "DTF_h0_PX"         : "DTF_FUN(CHILD(CHILD(PX,1),1),True)"
    , "DTF_h0_PY"         : "DTF_FUN(CHILD(CHILD(PY,1),1),True)"
    , "DTF_h0_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,1),1),True)"
    , "DTF_h1_P"          : "DTF_FUN(CHILD(CHILD(P,2),1),True)"
    , "DTF_h1_PT"         : "DTF_FUN(CHILD(CHILD(PT,2),1),True)"
    , "DTF_h1_E"          : "DTF_FUN(CHILD(CHILD(E,2),1),True)"
    , "DTF_h1_PX"         : "DTF_FUN(CHILD(CHILD(PX,2),1),True)"
    , "DTF_h1_PY"         : "DTF_FUN(CHILD(CHILD(PY,2),1),True)"
    , "DTF_h1_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,2),1),True)"
    , "DTF_h2_P"          : "DTF_FUN(CHILD(CHILD(P,3),1),True)"
    , "DTF_h2_PT"         : "DTF_FUN(CHILD(CHILD(PT,3),1),True)"
    , "DTF_h2_E"          : "DTF_FUN(CHILD(CHILD(E,3),1),True)"
    , "DTF_h2_PX"         : "DTF_FUN(CHILD(CHILD(PX,3),1),True)"
    , "DTF_h2_PY"         : "DTF_FUN(CHILD(CHILD(PY,3),1),True)"
    , "DTF_h2_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,3),1),True)"
    , "DTF_h3_P"          : "DTF_FUN(CHILD(CHILD(P,4),1),True)"
    , "DTF_h3_PT"         : "DTF_FUN(CHILD(CHILD(PT,4),1),True)"
    , "DTF_h3_E"          : "DTF_FUN(CHILD(CHILD(E,4),1),True)"
    , "DTF_h3_PX"         : "DTF_FUN(CHILD(CHILD(PX,4),1),True)"
    , "DTF_h3_PY"         : "DTF_FUN(CHILD(CHILD(PY,4),1),True)"
    , "DTF_h3_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,4),1),True)"
    ,"MAXDOCA" : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
    ,"deltaM": "DTF_FUN(M, True) - DTF_FUN(CHILDFUN(M,'D0'==ABSID), True)"
    }

# Prerequisites to get the trigger info

tttt = TupleToolTISTOS()



l0TriggerLines=[ "L0HadronDecision"
                ,"L0MuonDecision"
                ,"L0DiMuonDecision"
                ,"L0ElectronDecision"
                ,"L0PhotonDecision"]

hlt1TriggerLines=['Hlt1SingleMuonHighPTDecision','Hlt1MultiDiMuonNoIPDecision','Hlt1DiMuonNoL0Decision','Hlt1DiMuonNoIPDecision','Hlt1TwoTrackMVADecision','Hlt1TrackMuonNoSPDDecision','Hlt1TrackMVADecision','Hlt1DiMuonHighMassDecision', 'Hlt1DiMuonLowMassDecision', 'Hlt1SingleMuonNoIPDecision', 'Hlt1SingleMuonHighPTDecision', 'Hlt1TrackAllL0Decision', 'Hlt1TrackMuonDecision', 'Hlt1TrackPhotonDecision', 'Hlt1L0AnyDecision', 'Hlt1GlobalDecision' , 'Hlt1TrackMVALooseDecision', 'Hlt1TwoTrackMVALooseDecision' , 'Hlt1TrackMuonMVADecision']



hlt2TriggerLines=['Hlt2SingleMuonDecision','Hlt2DiMuonDetachedDecision', 'Hlt2CharmSemilepD2HMuMuDecision', 'Hlt2DiMuonDetachedDecision','Hlt2CharmSemilep3bodyD2PiMuMuDecision', 'Hlt2CharmSemilep3bodyD2KMuMuDecision', 'Hlt2CharmSemilep3bodyD2PiMuMuSSDecision', 'Hlt2CharmSemilep3bodyD2KMuMuSSDecision', 'Hlt2CharmSemilepD02PiPiMuMuDecision', 'Hlt2CharmSemilepD02KKMuMuDecision','Hlt2CharmSemilepD02KPiMuMuDecision','Hlt2CharmHadD02HHHHDst_4piDecision','Hlt2CharmHadD02HHHHDst_K3piDecision','Hlt2CharmHadD02HHHHDst_KKpipiDecision','Hlt2CharmHadD02HHXDst_hhXDecision', 'Hlt2CharmHadD02HHXDst_hhXWideMassDecision', 'Hlt2CharmHadD02HHXDst_BaryonhhXDecision', 'Hlt2CharmHadD02HHXDst_BaryonhhXWideMassDecision','Hlt2CharmHadD02HHXDst_LeptonhhXDecision', 'Hlt2CharmHadD02HHXDst_LeptonhhXWideMassDecision','Hlt2CharmHadD02HHHH_K3piDecision', 'Hlt2CharmHadD02HHHH_K3piWideMassDecision','Hlt2CharmHadD02HHHH_KKpipiDecision', 'Hlt2CharmHadD02HHHH_KKpipiWideMassDecision','Hlt2CharmHadD02HHHH_4piDecision', 'Hlt2CharmHadD02HHHH_4piWideMassDecision',
                 'Hlt2RareCharmD02KKMuMuFilterDecision',
                  'Hlt2RareCharmD02KPiDecision',
                  'Hlt2RareCharmD02MuMuDecision',
                  'Hlt2RareCharmD02PiPieeFilterDecision',
                  'Hlt2RareCharmD02KKMueFilterDecision',
                  'Hlt2RareCharmLc2PMueFilterDecision',
                  'Hlt2RareCharmD02PiPiDecision',
                  'Hlt2RareCharmD2KMuMuSSFilterDecision',
                  'Hlt2RareCharmD2PiMueFilterDecision',
                  'Hlt2RareCharmD2KMueFilterDecision',
                  'Hlt2RareCharmD02KPiMuMuFilterDecision',
                  'Hlt2RareCharmD2KMuMuFilterDecision',
                  'Hlt2RareCharmD2KeeFilterDecision',
                  'Hlt2RareCharmD02KPiMueFilterDecision',
                  'Hlt2RareCharmD02KPieeFilterDecision',
                  'Hlt2RareCharmD02KKeeFilterDecision',
                  'Hlt2RareCharmD2PiMuMuFilterDecision',
                  'Hlt2RareCharmLc2PeeFilterDecision',
                  'Hlt2RareCharmD2PieeFilterDecision',
                  'Hlt2RareCharmD02KPiMuMuSSFilterDecision',
                  'Hlt2RareCharmD2PiMuMuSSFilterDecision',
                  'Hlt2RareCharmLc2PMuMuSSFilterDecision',
                  'Hlt2RareCharmD02PiPiMuMuFilterDecision',
                  'Hlt2RareCharmD02EMuDecision',
                  'Hlt2RareCharmD02KMuDecision',
                  'Hlt2RareCharmLc2PMuMuFilterDecision',
                  'Hlt2RareCharmD02PiPiMueFilterDecision',
                  'Hlt2RareCharmD2PieeSSFilterDecision',
                  'Hlt2RareCharmD2KeeSSFilterDecision',
                  'Hlt2RareCharmD2PiMueSSFilterDecision',
                  'Hlt2RareCharmD2KMueSSFilterDecision',
                  'Hlt2RareCharmD02MuMuDecision',
                  'Hlt2RareCharmD02PiPiDecision',
                  'Hlt2RareCharmD02KPiDecision',
                  'Hlt2RareCharmD02KMuDecision',
                  'Hlt2RareCharmD02EMuDecision',
                  'Hlt2RareCharmD2PiMuMuOSDecision',
                  'Hlt2RareCharmD2PiMuMuSSDecision',
                  'Hlt2RareCharmD2PiMuMuWSDecision',
                  'Hlt2RareCharmD2KMuMuOSDecision',
                  'Hlt2RareCharmD2KMuMuSSDecision',
                  'Hlt2RareCharmD2KMuMuWSDecision',
                  'Hlt2RareCharmD2PiEEOSDecision',
                  'Hlt2RareCharmD2PiEESSDecision',
                  'Hlt2RareCharmD2PiEEWSDecision',
                  'Hlt2RareCharmD2KEEOSDecision',
                  'Hlt2RareCharmD2KEESSDecision',
                  'Hlt2RareCharmD2KEEWSDecision',
                  'Hlt2RareCharmD2PiMuEOSDecision',
                  'Hlt2RareCharmD2PiMuESSDecision',
                  'Hlt2RareCharmD2PiMuEWSDecision',
                  'Hlt2RareCharmD2PiEMuOSDecision',
                  'Hlt2RareCharmD2KMuEOSDecision',
                  'Hlt2RareCharmD2KMuESSDecision',
                  'Hlt2RareCharmD2KMuEWSDecision',
                  'Hlt2RareCharmD2KEMuOSDecision',
                  'Hlt2RareCharmLc2PMuMuDecision',
                  'Hlt2RareCharmLc2PMuMuSSDecision',
                  'Hlt2RareCharmLc2PeeDecision',
                  'Hlt2RareCharmLc2PMueDecision',
                  'Hlt2RareCharmD02PiPiMuMuDecision',
                  'Hlt2RareCharmD02KKMuMuDecision',
                  'Hlt2RareCharmD02KPiMuMuDecision',
                  'Hlt2RareCharmD02PiPieeDecision',
                  'Hlt2RareCharmD02KKeeDecision',
                  'Hlt2RareCharmD02KPieeDecision',
                  'Hlt2RareCharmD02PiPiMueDecision',
                  'Hlt2RareCharmD02KKMueDecision',
                  'Hlt2RareCharmD02KPiMueDecision',
                  'Hlt2RareCharmD02KPiMuMuSSDecision',
                  'Hlt2RareCharmD2KMueSSDecision']




triggerLines=l0TriggerLines+hlt1TriggerLines+hlt2TriggerLines



TTTI = TupleToolTrackIsolation("TTTI")
tos=TupleToolTISTOS("tos")


###################################################################
###        4-body decays with TAG
###    
####################################################################

############ Creation of the D*-> D->KKmumu ntuple #######################
# Def
Tuple16 = DecayTreeTuple("DstD2KKMuMu")

# Trigger
Tuple16.addTool(tttt)

# Get KKmumu candidates and start the ntuple
Tuple16.Inputs = [ 'Phys/DstarPromptWithD02HHLLLine' ]
Tuple16.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K+ ^K- ^mu+ ^mu- ) ^pi+ ]CC"
Tuple16.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ K- mu+ mu- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K+ ^K- mu+ mu- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ K- ^mu+ mu- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K+ K- mu+ ^mu- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K+ K- mu+ mu- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ K- mu+ mu- ) pi+ ]CC"
    ,"Dst"          : "[D*(2010)+ -> (D0 -> K+ K- mu+ mu- ) pi+ ]CC"
                }


Tuple16.ToolList = List_of_Tools

# Additional variables, that do not come by default
Tuple16.addTool(TupleToolDecay, name="D")
Tuple16.addTool(TupleToolDecay, name="Dst")         


Tuple16.addTool(TupleToolPid,name="TupleToolPid")
Tuple16.TupleToolPid.Verbose = True

Tuple16.addTool(TupleToolGeometry,name="TupleToolGeometry")
Tuple16.TupleToolGeometry.Verbose = True

Tuple16.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
Tuple16.TupleToolTrackInfo.Verbose = True
 

##Trigger
Tuple16.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.D.addTool(tos)
Tuple16.D.tos.VerboseL0=True
Tuple16.D.tos.VerboseHlt1=True
Tuple16.D.tos.VerboseHlt2=True
Tuple16.D.tos.TriggerList = triggerLines

Tuple16.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.Dst.addTool(tos)
Tuple16.Dst.tos.VerboseL0=True
Tuple16.Dst.tos.VerboseHlt1=True
Tuple16.Dst.tos.VerboseHlt2=True
Tuple16.Dst.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolDecay, name="h0")
Tuple16.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.h0.addTool(tos)
Tuple16.h0.tos.VerboseL0=True
Tuple16.h0.tos.VerboseHlt1=True
Tuple16.h0.tos.VerboseHlt2=True
Tuple16.h0.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolDecay, name="h1")
Tuple16.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.h1.addTool(tos)
Tuple16.h1.tos.VerboseL0=True
Tuple16.h1.tos.VerboseHlt1=True
Tuple16.h1.tos.VerboseHlt2=True
Tuple16.h1.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolDecay, name="mu0")
Tuple16.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.mu0.addTool(tos)
Tuple16.mu0.tos.VerboseL0=True
Tuple16.mu0.tos.VerboseHlt1=True
Tuple16.mu0.tos.VerboseHlt2=True
Tuple16.mu0.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolDecay, name="mu1")
Tuple16.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.mu1.addTool(tos)
Tuple16.mu1.tos.VerboseL0=True
Tuple16.mu1.tos.VerboseHlt1=True
Tuple16.mu1.tos.VerboseHlt2=True
Tuple16.mu1.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolL0Calo())
Tuple16.TupleToolL0Calo.WhichCalo = "HCAL"

####### Get the GEC variables #############################################

from Configurables import LoKi__Hybrid__EvtTupleTool
LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple")
LoKi_EvtTuple.VOID_Variables = {
    # track information
    "nLong"       : "RECSUMMARY(LHCb.RecSummary.nLongTracks      , -999, '', False )"
    ,"nUpstream"   : "RECSUMMARY(LHCb.RecSummary.nUpstreamTracks  , -999, '', False )"
    ,"nDownstream" : "RECSUMMARY(LHCb.RecSummary.nDownstreamTracks, -999, '', False )"
    ,"nBackward"   : "RECSUMMARY(LHCb.RecSummary.nBackTracks      , -999, '', False )" 
    ,"nMuon"       : "RECSUMMARY(LHCb.RecSummary.nMuonTracks      , -999, '', False )"
    ,"nVELO"       : "RECSUMMARY(LHCb.RecSummary.nVeloTracks      , -999, '', False )"
    ,"nTracks"         : "RECSUMMARY( LHCb.RecSummary.nTracks,-1,'/Event/Rec/Summary',False )"
     # pileup
    ,"nPVs"        : "RECSUMMARY(LHCb.RecSummary.nPVs, -999, '', False )"
     
     # tracking multiplicities
    ,"nSpdDigits"  : "RECSUMMARY(LHCb.RecSummary.nSPDhits,    -999, '', False )"
    ,"nITClusters" : "RECSUMMARY(LHCb.RecSummary.nITClusters, -999, '', False )"
    ,"nTTClusters" : "RECSUMMARY(LHCb.RecSummary.nTTClusters, -999, '', False )"
    }

LoKi_EvtTuple.Preambulo +=['from LoKiTracks.decorators import *',
                           'from LoKiNumbers.decorators import *',
                           'from LoKiCore.functions  import *' ]
    
# Put the GECs in the ntuples !!


Tuple16.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
Tuple16.addTool(LoKi_EvtTuple)

############ Creation of the D*-> D->PiPimumu ntuple #######################
# Def
Tuple17 = Tuple16.clone("DstD2PiPiMuMu")
# Get KKmumu candidates and start the ntuple
Tuple17.Inputs = [ 'Phys/DstarPromptWithD02HHLLLine' ]
Tuple17.Decay =  "[ D*(2010)+ -> ^(D0 -> ^pi+ ^pi- ^mu+ ^mu- ) ^pi+ ]CC"
Tuple17.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^pi+ pi- mu+ mu- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> pi+ ^pi- mu+ mu- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> pi+ pi- ^mu+ mu- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> pi+ pi- mu+ ^mu- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> pi+ pi- mu+ mu- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> pi+ pi- mu+ mu- ) pi+ ]CC"
    ,"Dst"          : "[D*(2010)+ -> (D0 -> pi+ pi- mu+ mu- ) pi+ ]CC"
                }



############ Creation of the D*-> D->K-Pi+mumu ntuple #######################
# Def
Tuple18 = Tuple16.clone("DstD2KPiMuMu")


# Get KKmumu candidates and start the ntuple
Tuple18.Inputs = [ 'Phys/DstarPromptWithD02HHLLLine' ]
Tuple18.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^mu+ ^mu- ) ^pi+ ]CC"
Tuple18.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ mu+ mu- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K- ^pi+ mu+ mu- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^mu+ mu- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ ^mu- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ mu- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K- pi+ mu+ mu- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ mu- ) pi+ ]CC"
                }


############ Creation of the D*-> D->K+Pi-mumu WS ntuple #######################
# Def
Tuple19 = Tuple16.clone("DstD2KPiMuMuWS")

# Get KKmumu candidates and start the ntuple
Tuple19.Inputs = [ 'Phys/DstarPromptWithD02HHLLLine' ]
Tuple19.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^mu+ ^mu- ) ^pi+ ]CC"
Tuple19.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- mu+ mu- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K+ ^pi- mu+ mu- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ pi- ^mu+ mu- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ ^mu- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ mu- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ pi- mu+ mu- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ mu- ) pi+ ]CC"
                }

##add variables for all decays
Tuple16.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu"]
Tuple16.D.addTool(LoKi_All_hhmumu)
Tuple17.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu"]
Tuple17.D.addTool(LoKi_All_hhmumu)
Tuple18.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu"]
Tuple18.D.addTool(LoKi_All_hhmumu)
Tuple19.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu"]
Tuple19.D.addTool(LoKi_All_hhmumu)

Tuple20 = Tuple16.clone("DstD2KPiPiPi")

# Get KKmumu candidates and start the ntuple
Tuple20.Inputs = [ 'Phys/DstarPromptWithD02HHMuMuControlLine' ]
Tuple20.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi- ^pi+ ) ^pi+ ]CC"
Tuple20.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ pi- pi+ ) pi+ ]CC"
    ,"h1"       : "[ D*(2010)+ -> (D0 -> K- ^pi+ pi- pi+ ) pi+ ]CC"    
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^pi- pi+ ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K- pi+ pi- ^pi+ ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K- pi+ pi- pi+ ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K- pi+ pi- pi+ ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K- pi+ pi- pi+ ) pi+ ]CC"
                }

Tuple21 = Tuple16.clone("DstD2PiPiPiPi")

# Get KKmumu candidates and start the ntuple
Tuple21.Inputs = [ 'Phys/DstarPromptWithD02HHMuMuControlLine' ]
Tuple21.Decay =  "[ D*(2010)+ -> ^(D0 -> ^pi- ^pi+ ^pi+ ^pi- ) ^pi+ ]CC"
Tuple21.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^pi- pi+ pi+ pi- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> pi- ^pi+ pi+ pi- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> pi- pi+ ^pi+ pi- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> pi- pi+ pi+ ^pi- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> pi- pi+ pi+ pi- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> pi- pi+ pi+ pi- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> pi- pi+ pi+ pi- ) pi+ ]CC"
                }


Tuple22 = Tuple16.clone("DstD2KKPiPi")

# Get KKmumu candidates and start the ntuple
Tuple22.Inputs = [ 'Phys/DstarPromptWithD02HHMuMuControlLine' ]
Tuple22.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K- ^K+ ^pi+ ^pi- ) ^pi+ ]CC"
Tuple22.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- K+ pi+ pi- ) pi+ ]CC"
    ,"h1"       : "[ D*(2010)+ -> (D0 -> K- ^K+ pi+ pi- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K- K+ ^pi+ pi- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K- K+ pi+ ^pi- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K- K+ pi+ pi- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K- K+ pi+ pi- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K- K+ pi+ pi- ) pi+ ]CC"
                }

Tuple23 = Tuple16.clone("DstD2KPiPiPi_WS")

Tuple23.Inputs = [ 'Phys/DstarPromptWithD02HHMuMuControlLine' ]
Tuple23.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^pi- ^pi+ ) ^pi+ ]CC"
Tuple23.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- pi- pi+ ) pi+ ]CC"
    ,"h1"       : "[ D*(2010)+ -> (D0 -> K+ ^pi- pi- pi+ ) pi+ ]CC"    
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ pi- ^pi- pi+ ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K+ pi- pi- ^pi+ ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K+ pi- pi- pi+ ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ pi- pi- pi+ ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K+ pi- pi- pi+ ) pi+ ]CC"
                }

Tuple16.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple16.Dst.addTool(LoKi_Cone)
Tuple17.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple17.Dst.addTool(LoKi_Cone)
Tuple18.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple18.Dst.addTool(LoKi_Cone)
Tuple19.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple19.Dst.addTool(LoKi_Cone)

Tuple16.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple16.Dst.addTool(LoKi_All_hhmumu_Dst)
Tuple17.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple17.Dst.addTool(LoKi_All_hhmumu_Dst)
Tuple18.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple18.Dst.addTool(LoKi_All_hhmumu_Dst)
Tuple19.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple19.Dst.addTool(LoKi_All_hhmumu_Dst)

Tuple20.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_had"]
Tuple20.Dst.addTool(LoKi_Cone_had)
Tuple21.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_had"]
Tuple21.Dst.addTool(LoKi_Cone_had)
Tuple22.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_had"]
Tuple22.Dst.addTool(LoKi_Cone_had)
Tuple23.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_had"]
Tuple23.Dst.addTool(LoKi_Cone_had)

Tuple20.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst_had"]
Tuple20.Dst.addTool(LoKi_All_hhmumu_Dst_had)
Tuple21.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst_had"]
Tuple21.Dst.addTool(LoKi_All_hhmumu_Dst_had)
Tuple22.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst_had"]
Tuple22.Dst.addTool(LoKi_All_hhmumu_Dst_had)
Tuple23.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst_had"]
Tuple23.Dst.addTool(LoKi_All_hhmumu_Dst_had)

#############################################################################
##
##   electron modes
##
########################################################

############ Creation of the D*-> D->K-K+ee ntuple #######################
# Def
Tuple24 = Tuple16.clone("DstD2KKee")


# Get KKmumu candidates and start the ntuple
Tuple24.Inputs = [ 'Phys/DstarPromptWithD02HHLLLine' ]
Tuple24.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K- ^K+ ^e+ ^e- ) ^pi+ ]CC"
Tuple24.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- K+ e+ e- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K- ^K+ e+ e- ) pi+ ]CC"
    ,"e0"     : "[ D*(2010)+ -> (D0 -> K- K+ ^e+ e- ) pi+ ]CC"
    ,"e1"        : "[ D*(2010)+ -> (D0 -> K- K+ e+ ^e- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K- K+ e+ e- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K- K+ e+ e- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K- K+ e+ e- ) pi+ ]CC"
                }



############ Creation of the D*-> D->pi-pi+ee ntuple #######################
# Def
Tuple25 = Tuple16.clone("DstD2PiPiee")


# Get KKmumu candidates and start the ntuple
Tuple25.Inputs = [ 'Phys/DstarPromptWithD02HHLLLine' ]
Tuple25.Decay =  "[ D*(2010)+ -> ^(D0 -> ^pi- ^pi+ ^e+ ^e- ) ^pi+ ]CC"
Tuple25.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^pi- pi+ e+ e- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> pi- ^pi+ e+ e- ) pi+ ]CC"
    ,"e0"     : "[ D*(2010)+ -> (D0 -> pi- pi+ ^e+ e- ) pi+ ]CC"
    ,"e1"        : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ ^e- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ e- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> pi- pi+ e+ e- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ e- ) pi+ ]CC"
                }

############ Creation of the D*-> D->K-pi+ee ntuple #######################
# Def
Tuple26 = Tuple16.clone("DstD2KPiee")

# Get KKmumu candidates and start the ntuple
Tuple26.Inputs = [ 'Phys/DstarPromptWithD02HHLLLine' ]
Tuple26.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^e+ ^e- ) ^pi+ ]CC"
Tuple26.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ e+ e- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K- ^pi+ e+ e- ) pi+ ]CC"
    ,"e0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^e+ e- ) pi+ ]CC"
    ,"e1"        : "[ D*(2010)+ -> (D0 -> K- pi+ e+ ^e- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K- pi+ e+ e- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K- pi+ e+ e- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K- pi+ e+ e- ) pi+ ]CC"
                }


############ Creation of the D*-> D->K-pi+ee ntuple #######################
# Def
Tuple27 = Tuple16.clone("DstD2KPieeWS")

# Get KKmumu candidates and start the ntuple
Tuple27.Inputs = [ 'Phys/DstarPromptWithD02HHLLLine' ]
Tuple27.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^e+ ^e- ) ^pi+ ]CC"
Tuple27.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- e+ e- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K+ ^pi- e+ e- ) pi+ ]CC"
    ,"e0"     : "[ D*(2010)+ -> (D0 -> K+ pi- ^e+ e- ) pi+ ]CC"
    ,"e1"        : "[ D*(2010)+ -> (D0 -> K+ pi- e+ ^e- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K+ pi- e+ e- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ pi- e+ e- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K+ pi- e+ e- ) pi+ ]CC"
                }


Tuple24.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple24.Dst.addTool(LoKi_Cone)
Tuple25.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple25.Dst.addTool(LoKi_Cone)
Tuple26.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple26.Dst.addTool(LoKi_Cone)
Tuple27.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple27.Dst.addTool(LoKi_Cone)

Tuple24.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple24.Dst.addTool(LoKi_All_hhmumu_Dst)
Tuple25.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple25.Dst.addTool(LoKi_All_hhmumu_Dst)
Tuple26.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple26.Dst.addTool(LoKi_All_hhmumu_Dst)
Tuple27.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple27.Dst.addTool(LoKi_All_hhmumu_Dst)

from Configurables import TupleToolBremInfo
from Configurables import TupleToolProtoPData

ttBrem1 = Tuple24.addTool(TupleToolBremInfo("TupleToolBremInfo"))
ttBrem1.Particle = ["e+","e-"]
ttBrem1.Verbose = True
ttPPD1 = Tuple24.addTool(TupleToolProtoPData("TupleToolProtoPData"))
ttPPD1.DataList = ["VeloCharge","CaloEoverP", "CaloEcalChi2", "CaloPrsE", "CaloHcalE", 
                  "EcalPIDe", "PrsPIDe", "HcalPIDe", "CaloEcalE"]

Tuple24.ToolList += ["TupleToolBremInfo","TupleToolProtoPData"]

ttBrem2 = Tuple25.addTool(TupleToolBremInfo("TupleToolBremInfo"))
ttBrem2.Particle = ["e+","e-"]
ttBrem2.Verbose = True
ttPPD2 = Tuple25.addTool(TupleToolProtoPData("TupleToolProtoPData"))
ttPPD2.DataList = ["VeloCharge","CaloEoverP", "CaloEcalChi2", "CaloPrsE", "CaloHcalE", 
                  "EcalPIDe", "PrsPIDe", "HcalPIDe", "CaloEcalE"]

Tuple25.ToolList += ["TupleToolBremInfo","TupleToolProtoPData"]

ttBrem3 = Tuple26.addTool(TupleToolBremInfo("TupleToolBremInfo"))
ttBrem3.Particle = ["e+","e-"]
ttBrem3.Verbose = True
ttPPD3 = Tuple26.addTool(TupleToolProtoPData("TupleToolProtoPData"))
ttPPD3.DataList = ["VeloCharge","CaloEoverP", "CaloEcalChi2", "CaloPrsE", "CaloHcalE", 
                  "EcalPIDe", "PrsPIDe", "HcalPIDe", "CaloEcalE"]
Tuple26.ToolList += ["TupleToolBremInfo","TupleToolProtoPData"]

ttBrem4 = Tuple27.addTool(TupleToolBremInfo("TupleToolBremInfo"))
ttBrem4.Particle = ["e+","e-"]
ttBrem4.Verbose = True
ttPPD4 = Tuple27.addTool(TupleToolProtoPData("TupleToolProtoPData"))
ttPPD4.DataList = ["VeloCharge","CaloEoverP", "CaloEcalChi2", "CaloPrsE", "CaloHcalE", 
                  "EcalPIDe", "PrsPIDe", "HcalPIDe", "CaloEcalE"]
Tuple27.ToolList += ["TupleToolBremInfo","TupleToolProtoPData"]

 

############ Execution part (ie Sequences and DV settings) #####
from Configurables import GaudiSequencer


Seq_KK_tag = GaudiSequencer("Seq_KK_tag")
Seq_KK_tag.Members  += [ Tuple16 ]

Seq_PiPi_tag = GaudiSequencer("Seq_PiPi_tag")
Seq_PiPi_tag.Members  += [ Tuple17 ]

Seq_KPi_tag = GaudiSequencer("Seq_KPi_tag")
Seq_KPi_tag.Members  += [ Tuple18 ]

Seq_KPi_WS_tag = GaudiSequencer("Seq_KPi_WS_tag")
Seq_KPi_WS_tag.Members  += [ Tuple19 ]

Seq_K3pi_tag = GaudiSequencer("Seq_K3pi_tag")
Seq_K3pi_tag.Members  += [ Tuple20 ]

Seq_4Pi_tag = GaudiSequencer("Seq_4Pi_tag")
Seq_4Pi_tag.Members  += [ Tuple21 ]

Seq_2K2Pi_tag = GaudiSequencer("Seq_2K2Pi_tag")
Seq_2K2Pi_tag.Members  += [ Tuple22 ]

Seq_K3pi_WS_tag = GaudiSequencer("Seq_K3pi_WS_tag")
Seq_K3pi_WS_tag.Members  += [ Tuple23 ]

Seq_KKee_tag = GaudiSequencer("Seq_KKee_tag")
Seq_KKee_tag.Members  += [ Tuple24 ]

Seq_PiPiee_tag = GaudiSequencer("Seq_PiPiee_tag")
Seq_PiPiee_tag.Members  += [ Tuple25 ]

Seq_KPiee_tag = GaudiSequencer("Seq_KPiee_tag")
Seq_KPiee_tag.Members  += [ Tuple26 ]

Seq_KPiee_WS_tag = GaudiSequencer("Seq_KPiee_WS_tag")
Seq_KPiee_WS_tag.Members  += [ Tuple27 ]


from Configurables import DaVinci

DaVinci().TupleFile = "RARECHARM_4BODIES.ROOT"             # Ntuple

 
DaVinci().UserAlgorithms += [Seq_KK_tag]

DaVinci().UserAlgorithms += [Seq_PiPi_tag]

DaVinci().UserAlgorithms += [Seq_KPi_tag]

DaVinci().UserAlgorithms += [Seq_KPi_WS_tag]

DaVinci().UserAlgorithms += [Seq_K3pi_tag]

DaVinci().UserAlgorithms += [Seq_4Pi_tag]

DaVinci().UserAlgorithms += [Seq_2K2Pi_tag]

DaVinci().UserAlgorithms += [Seq_K3pi_WS_tag]

DaVinci().UserAlgorithms += [Seq_KKee_tag]

DaVinci().UserAlgorithms += [Seq_PiPiee_tag]

DaVinci().UserAlgorithms += [Seq_KPiee_tag]

DaVinci().UserAlgorithms += [Seq_KPiee_WS_tag]




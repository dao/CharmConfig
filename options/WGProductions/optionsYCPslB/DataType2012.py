
year      = '2012'

from Configurables import DaVinci, CondDB
DaVinci (  DataType = year,
           Simulation = False,
           Lumi = True,
           EvtMax = -1,
           PrintFreq = 1000,
           InputType="DST"
        )
CondDB  ( LatestGlobalTagByDataType = year )


import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

from Configurables import TupleToolDecay, TupleToolTISTOS, TupleToolTrigger, L0TriggerTisTos, TriggerTisTos

triggerList = [
    "L0MuonDecision",
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2TopoMu2BodyBBDTDecision",
    "Hlt2TopoMu3BodyBBDTDecision",
    "Hlt2TopoMu4BodyBBDTDecision",
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo4BodyBBDTDecision"]

mu_triggerList = [
    "L0MuonDecision",
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt2SingleMuonDecision"]

B_triggerList = [
    "Hlt2TopoMu2BodyBBDTDecision",
    "Hlt2TopoMu3BodyBBDTDecision",
    "Hlt2TopoMu4BodyBBDTDecision",
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo4BodyBBDTDecision"]

stable_variables = { "Q" : "Q",
              "PX" : "PX",
              "PY" : "PY",
              "PZ" : "PZ",
              "IP" : "BPVIP()",
              "IPChi2" : "BPVIPCHI2()",
              "ProbNNe" : "PROBNNe",
              "ProbNNmu" : "PROBNNmu",
              "ProbNNpi" : "PROBNNpi",
              "ProbNNk" : "PROBNNk",
              "ProbNNp" : "PROBNNp",
              "ProbNNghost" : "PROBNNghost",
              "IsMuon" : "switch(ISMUON,1,0)" }

D_variables = { "MM" : "MM", "PX" : "PX", "PY" : "PY", "PZ" : "PZ",
                "DOCAMAX" : "DOCAMAX", "FDChi2" : "BPVVDCHI2",
                "IP" : "BPVIP()", "IPChi2" : "BPVIPCHI2()", "VChi2" : "VFASPF(VCHI2/VDOF)",
                "VX" : "VFASPF(VX)", "VY" : "VFASPF(VY)", "VZ" : "VFASPF(VZ)",
                "PVX" : "BPV(VX)", "PVY" : "BPV(VY)", "PVZ" : "BPV(VZ)", "M" : "M", "DTF_M" : "DTF_FUN ( M , False )" }

B_variables = { "MM" : "MM", "PX" : "PX", "PY" : "PY", "PZ" : "PZ",
                "DOCA" : "DOCA(1,2)", "FDChi2" : "BPVVDCHI2", "VChi2" : "VFASPF(VCHI2/VDOF)",
                "VX" : "VFASPF(VX)", "VY" : "VFASPF(VY)", "VZ" : "VFASPF(VZ)",
                "PVX" : "BPV(VX)", "PVY" : "BPV(VY)", "PVZ" : "BPV(VZ)", "M" : "M", "DTF_D_ct" : "DTF_CTAU( 'D+' == ABSID , False )" }


def fillTuple( tuple ) :
        tuple.ToolList = [ "TupleToolEventInfo", "TupleToolTrigger", "TupleToolPrimaries" ]
        tuple.addTool(TupleToolTrigger())
        tuple.TupleToolTrigger.TriggerList = triggerList
        tuple.TupleToolTrigger.VerboseL0 = True
        tuple.TupleToolTrigger.VerboseHlt1 = True
        tuple.TupleToolTrigger.VerboseHlt2 = True

        tuple.addTool(TupleToolDecay, name="Mu")
        tuple.Mu.ToolList += [ "TupleToolTISTOS" ]
        tuple.Mu.addTool(TupleToolTISTOS())
        tuple.Mu.TupleToolTISTOS.addTool(L0TriggerTisTos())
        tuple.Mu.TupleToolTISTOS.addTool(TriggerTisTos())
        tuple.Mu.TupleToolTISTOS.TriggerList = mu_triggerList
        tuple.Mu.TupleToolTISTOS.Verbose = True
        tuple.Mu.TupleToolTISTOS.VerboseL0 = True
        tuple.Mu.TupleToolTISTOS.VerboseHlt1 = True
        MuLoKi = tuple.Mu.addTupleTool("LoKi::Hybrid::TupleTool/MuLoKi")
        MuLoKi.Variables = stable_variables

        tuple.addTool(TupleToolDecay, name="K1")
        K1LoKi = tuple.K1.addTupleTool("LoKi::Hybrid::TupleTool/K1LoKi")
        K1LoKi.Variables = stable_variables

        tuple.addTool(TupleToolDecay, name="K2")
        K2LoKi = tuple.K2.addTupleTool("LoKi::Hybrid::TupleTool/K2LoKi")
        K2LoKi.Variables = stable_variables

        tuple.addTool(TupleToolDecay, name="Pi")
        PiLoKi = tuple.Pi.addTupleTool("LoKi::Hybrid::TupleTool/PiLoKi")
        PiLoKi.Variables = stable_variables

        tuple.addTool(TupleToolDecay, name="D")
        DLoKi = tuple.D.addTupleTool("LoKi::Hybrid::TupleTool/DLoKi")
        DLoKi.Variables = D_variables

        tuple.addTool(TupleToolDecay, name="B")
        tuple.B.ToolList += [ "TupleToolTISTOS" ]
        tuple.B.addTool(TupleToolTISTOS())
        tuple.B.TupleToolTISTOS.addTool(TriggerTisTos())
        tuple.B.TupleToolTISTOS.TriggerList = B_triggerList
        tuple.B.TupleToolTISTOS.Verbose = True
        tuple.B.TupleToolTISTOS.VerboseHlt2 = True
        BLoKi = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/BLoKi")
        BLoKi.Variables = B_variables



###### Momentum scaling                                                                                                                             
from Configurables import TrackScaleState
scaleSeq = GaudiSequencer("scaleSeq")
scaler = TrackScaleState('TrackStateScale')
scaleSeq.Members += [scaler]

userAlgos = []
userAlgos.append(scaleSeq)

#######################################################################
#
# Define a DecayTreeTuple
#
from DecayTreeTuple.Configuration import *

tuple = DecayTreeTuple("DMutuple")
tuple.TupleName = "DMutuple"
tuple.Inputs = ['/Event/Semileptonic/Phys/b2DsPhiPiMuXB2DMuNuXLine/Particles']
tuple.Decay = '[B0 -> ^(D- -> ^K+ ^K- ^pi-) ^mu+]CC'
tuple.Branches = {
   "B"  : "[B0 -> (D- ->  K+ K- pi-) mu+]CC",
   "D"  : "[B0 -> ^(D- ->  K+  K-  pi-)  mu+]CC",
   "Mu" : "[B0 -> ( D- ->  K+  K-  pi-) ^mu+]CC",
   "K1" : "[B0 -> ( D- -> ^K+  K-  pi-)  mu+]CC",
   "K2" : "[B0 -> ( D- ->  K+ ^K-  pi-)  mu+]CC",
   "Pi" : "[B0 -> ( D- ->  K+  K- ^pi-)  mu+]CC"
}

sstuple = DecayTreeTuple("SSDMutuple")
sstuple.TupleName = "SSDMutuple"
sstuple.Inputs = ['/Event/Semileptonic/Phys/b2DsPhiPiMuXB2DMuNuXLine/Particles']
sstuple.Decay = '[B0 -> ^(D- -> ^K+ ^K- ^pi-) ^mu-]CC'
sstuple.Branches = {
   "B"  : "[B0 -> (D- ->  K+ K- pi-) mu-]CC",
   "D"  : "[B0 -> ^(D- ->  K+  K-  pi-)  mu-]CC",
   "Mu" : "[B0 -> ( D- ->  K+  K-  pi-) ^mu-]CC",
   "K1" : "[B0 -> ( D- -> ^K+  K-  pi-)  mu-]CC",
   "K2" : "[B0 -> ( D- ->  K+ ^K-  pi-)  mu-]CC",
   "Pi" : "[B0 -> ( D- ->  K+  K- ^pi-)  mu-]CC"
}

tuples = [ tuple, sstuple ]
for t in tuples: fillTuple( t )

#######################################################################
#
# Configure the application
#
from Configurables import DaVinci

# build prefilter
from PhysConf.Filters import LoKi_Filters
filterAlg = LoKi_Filters( STRIP_Code = "HLT_PASS('Strippingb2DsPhiPiMuXB2DMuNuXLineDecision')" )
lokifilters = filterAlg.filters('stripFilter')
DaVinci().EventPreFilters = lokifilters
DaVinci().TupleFile = "SLYCP_KKpi.root"

# the selection sequence
for t in tuples:
    userAlgos.append(t)
DaVinci().UserAlgorithms = userAlgos


#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# output/2016-sim09b/DpTopiphi_phiTomumu_OS/
# lb-run LHCbDIRAC/prod ./create_WG_production.py --create --print \
#     --name='Charm D2hll 2016 MagDown 21173001 Sim09b' \
#     --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
#     --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21173001/ALLSTREAMS.DST'
# lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
#     --name='Charm D2hll 2016 MagUp 21173001 Sim09b' \
#     --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
#     --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21173001/ALLSTREAMS.DST'

# output/2016-sim09b/DpTopiphi_phiToee_OS/
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21123020 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123020/ALLSTREAMS.DST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21123020 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123020/ALLSTREAMS.DST'

# output/2016-sim09b/DpTopiphi_phiToemu_OS/
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21313000 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21313000/ALLSTREAMS.DST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21313000 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21313000/ALLSTREAMS.DST'

# output/2016-sim09b/DspToXphi_phiToemu_OS/
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23712012 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23712012/ALLSTREAMS.DST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23712012 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23712012/ALLSTREAMS.DST'

# output/2016-sim09b/DspToXphi_phiTomumu_OS/
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23712020 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23712020/ALLSTREAMS.DST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23712020 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23712020/ALLSTREAMS.DST'

# output/2016-sim09b/DspToXphi_phiToee_OS/
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23722012 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23722012/ALLSTREAMS.DST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23722012 Sim09b' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23722012/ALLSTREAMS.DST'

# output/2016/DpTopiphi_phiToee_OS/
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21123020 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123020/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21123020 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123020/ALLSTREAMS.MDST'

# output/2016/DpTopimumu_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113000 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113000/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113000 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113000/ALLSTREAMS.MDST'

# output/2016/DpToKmumu_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113001 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113001/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113001 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113001/ALLSTREAMS.MDST'

# output/2016/DpTopimumu_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113002 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113002/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113002 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113002/ALLSTREAMS.MDST'

# output/2016/DpToKmumu_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113003 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113003/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113003 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113003/ALLSTREAMS.MDST'

# output/2016/DpTopiee_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21123011 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123011/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21123011 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123011/ALLSTREAMS.MDST'

# output/2016/DpToKee_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21123010 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123010/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21123010 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123010/ALLSTREAMS.MDST'

# output/2016/DpToKee_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21123000 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123000/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21123000 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123000/ALLSTREAMS.MDST'

# output/2016/DpTopiee_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21123001 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123001/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21123001 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123001/ALLSTREAMS.MDST'

# output/2016/DpTopimue_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113016 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113016/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113016 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113016/ALLSTREAMS.MDST'

# output/2016/DpTopimue_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113036 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113036/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113036 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113036/ALLSTREAMS.MDST'

# output/2016/DpTopiemu_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113006 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113006/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113006 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113006/ALLSTREAMS.MDST'

# output/2016/DpToKmue_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113015 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113015/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113015 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113015/ALLSTREAMS.MDST'

# output/2016/DpToKmue_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113035 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113035/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113035 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113035/ALLSTREAMS.MDST'

# output/2016/DpToKemu_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 21113005 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113005/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 21113005 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113005/ALLSTREAMS.MDST'

# output/2016/DspTopimumu_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113000 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113000/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113000 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113000/ALLSTREAMS.MDST'

# output/2016/DspToKmumu_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113001 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113001/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113001 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113001/ALLSTREAMS.MDST'

# output/2016/DspTopimumu_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113003 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113003/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113003 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113003/ALLSTREAMS.MDST'

# output/2016/DspToKmumu_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113004 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113004/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113004 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113004/ALLSTREAMS.MDST'

# output/2016/DspToKee_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23123000 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123000/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23123000 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123000/ALLSTREAMS.MDST'

# output/2016/DspTopiee_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23123001 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123001/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23123001 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123001/ALLSTREAMS.MDST'

# output/2016/DspToKee_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23123010 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123010/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23123010 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123010/ALLSTREAMS.MDST'

# output/2016/DspTopiee_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23123011 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123011/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23123011 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123011/ALLSTREAMS.MDST'

# output/2016/DspToKemu_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113010 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113010/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113010 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113010/ALLSTREAMS.MDST'

# output/2016/DspToKmue_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113015 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113015/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113015 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113015/ALLSTREAMS.MDST'

# output/2016/DspToKmue_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113035 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113035/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113035 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113035/ALLSTREAMS.MDST'

# output/2016/DspTopiemu_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113011 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113011/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113011 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113011/ALLSTREAMS.MDST'

# output/2016/DspTopimue_OS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113016 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113016/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113016 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113016/ALLSTREAMS.MDST'

# output/2016/DspTopimue_SS
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagDown 23113036 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113036/ALLSTREAMS.MDST'
./create_WG_production.py --create --submit --print \
    --name='Charm D2hll 2016 MagUp 23113036 Sim09c' \
    --author='cburr' --step='133174' --step='133211' --dq-flag='ALL' \
    --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113036/ALLSTREAMS.MDST'

tuple_tools = [
    'TupleToolGeometry',
    'TupleToolKinematic',
    'TupleToolPid',
    'TupleToolANNPID',
    'TupleToolPrimaries',
    'TupleToolTrackInfo',
    'TupleToolBremInfo'
]

basic_loki_vars = {
    'ETA': 'ETA',
    'Loki_BPVIPCHI2': 'BPVIPCHI2()'
}

mother_loki_vars = {
    'Loki_BPVVDCHI2': 'BPVVDCHI2',
    'Loki_BPVDIRA': 'BPVDIRA',
    'Loki_BPVIPCHI2': 'BPVIPCHI2()',
    'Loki_DOCAMAX': 'DOCAMAX',
    'Loki_AMAXDOCA': "LoKi.Particles.PFunA(AMAXDOCA(''))",
    'Loki_AMINDOCA': "LoKi.Particles.PFunA(AMINDOCA(''))",
    'Loki_DOCACHI2MAX': 'DOCACHI2MAX',
    'Loki_VCHI2': 'VFASPF(VCHI2)',
    'Loki_VDOF': 'VFASPF(VDOF)',
    'Loki_VX': 'VFASPF(VX)',
    'Loki_VY': 'VFASPF(VY)',
    'Loki_VZ': 'VFASPF(VZ)',
    'Loki_SUMPT': 'SUMTREE(PT,  ISBASIC)',
    'Loki_BPVLTIME': "BPVLTIME()",
    'Loki_M12': 'M12',
    'Loki_M13': 'M13',
    'Loki_M23': 'M23',
}

cone_angles = [1.0, 1.5, 2.0]
cone_vars = [
    'CONEANGLE',
    'CONEMULT',
    'CONEPX',
    'CONEPY',
    'CONEPZ',
    'CONEPT',
    'CONEPXASYM',
    'CONEPYASYM',
    'CONEPZASYM',
    'CONEPASYM',
    'CONEPTASYM',
    'CONEDELTAETA',
    'CONEDELTAPHI'
]
ew_cone_angles = [0.0, 0.5, 1.0, 1.5]
ew_cone_vars = [
    'EWCONEANGLE',
    'EWCONEMULT',
    'EWCONEPX',
    'EWCONEPY',
    'EWCONEPZ',
    'EWCONEVP',
    'EWCONEVPT',
    'EWCONESPT',
    'EWCONETPT',
    'EWCONEMINPTE',
    'EWCONEMAXPTE',
    'EWCONEMINPTMU',
    'EWCONEMAXPTMU',
    'EWCONENMULT',
    'EWCONENPX',
    'EWCONENPY',
    'EWCONENPZ',
    'EWCONENVP',
    'EWCONENVPT',
    'EWCONENSP',
    'EWCONENSPT',
]
vertex_iso_variables = [
    'VTXISONUMVTX',
    'VTXISODCHI2ONETRACK',
    'VTXISODCHI2MASSONETRACK',
    'VTXISODCHI2TWOTRACK',
    'VTXISODCHI2MASSTWOTRACK'
]
vertex_iso_bdt_variables = [
    'VTXISOBDTHARDFIRSTVALUE',
    'VTXISOBDTHARDSECONDVALUE',
    'VTXISOBDTHARDTHIRDVALUE'
]
track_iso_bdt_variables = [
    'TRKISOBDTFIRSTVALUE',
    'TRKISOBDTSECONDVALUE',
    'TRKISOBDTTHIRDVALUE'
]
Bs2MuMuTrackIsolation_variables = [
    'BSMUMUTRACKPLUSISO',
    'BSMUMUTRACKPLUSISOTWO',
    'ISOTWOBODYQPLUS',
    'ISOTWOBODYMASSISOPLUS',
    'ISOTWOBODYCHI2ISOPLUS',
    'ISOTWOBODYISO5PLUS',
    'BSMUMUTRACKID',
    'BSMUMUTRACKTOPID'
]

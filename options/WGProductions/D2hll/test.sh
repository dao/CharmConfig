OPTIONS_DIR="\$CHARMCONFIGROOT/options/WGProductions/D2hll"

# 2015 Real
run_test \
    "output/2015/Real/" \
    "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/90000000/CHARM.MDST" \
    "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24/90000000/CHARM.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"

# 2016 Real
run_test \
    "output/2016/Real/" \
    "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28/90000000/CHARM.MDST" \
    "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28/90000000/CHARM.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"

# 2017 Real
run_test \
    "output/2017/Real/" \
    "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29/90000000/CHARM.MDST" \
    "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29/90000000/CHARM.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"

# 2016 MC
# Sim09b resonances
run_test \
    "output/2016-sim09b/DpTopiphi_phiTomumu_OS/" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21173001/ALLSTREAMS.DST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21173001/ALLSTREAMS.DST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016-sim09b/DpTopiphi_phiToee_OS/" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123020/ALLSTREAMS.DST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123020/ALLSTREAMS.DST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016-sim09b/DpTopiphi_phiToemu_OS/" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21313000/ALLSTREAMS.DST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21313000/ALLSTREAMS.DST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016-sim09b/DspToXphi_phiToemu_OS/" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23712012/ALLSTREAMS.DST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23712012/ALLSTREAMS.DST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016-sim09b/DspToXphi_phiTomumu_OS/" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23712020/ALLSTREAMS.DST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23712020/ALLSTREAMS.DST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016-sim09b/DspToXphi_phiToee_OS/" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23722012/ALLSTREAMS.DST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/23722012/ALLSTREAMS.DST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"


# Sim09c resonances
run_test \
    "output/2016/DpTopiphi_phiToee_OS/" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123020/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123020/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"

# Signal channels
run_test \
    "output/2016/DpTopimumu_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113000/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113000/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpToKmumu_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113001/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113001/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpTopimumu_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113002/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113002/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpToKmumu_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113003/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113003/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpTopiee_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123011/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123011/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpToKee_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123010/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123010/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpToKee_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123000/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123000/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpTopiee_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123001/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21123001/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpTopimue_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113016/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113016/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpTopimue_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113036/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113036/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpTopiemu_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113006/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113006/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpToKmue_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113015/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113015/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpToKmue_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113035/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113035/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DpToKemu_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113005/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/21113005/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspTopimumu_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113000/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113000/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspToKmumu_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113001/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113001/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspTopimumu_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113003/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113003/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspToKmumu_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113004/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113004/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspToKee_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123000/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123000/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspTopiee_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123001/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123001/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspToKee_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123010/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123010/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspTopiee_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123011/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123011/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspToKemu_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113010/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113010/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspToKmue_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113015/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113015/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspToKmue_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113035/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113035/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspTopiemu_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113011/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113011/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspTopimue_OS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113016/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113016/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"
run_test \
    "output/2016/DspTopimue_SS" \
    "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113036/ALLSTREAMS.MDST" \
    "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23113036/ALLSTREAMS.MDST" \
    "main_options.py" "Charm_D2hll_DVntuple.root" "133174"

"""
Stripping Filtering file for Walter Bonivento
@author Sam Gregson
@date   2012-06-01

PAY ATTENTION: If your line is prescaled in the stripping you might want to adjust that!
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

###########################################
###########################################

#Select the streams you are interested in - in this case just the charm complete event and MDST streams 

_charm_complete = quickBuild('CharmCompleteEvent')
_charm_micro    = quickBuild('Charm')

streams.append( _charm_complete )
streams.append( _charm_micro )

#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("Dst2D0Pi.Strip.Trig")

for stream in streams:                       # stream = "member" of streams
    AllStreams.appendLines(stream.lines)          # add all the lines associated with each of the streams


##############################################
##############################################

# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

###########################################
###########################################



from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )


#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )


#----------Include trigger filtering---------------------------

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (

L0DU_Code = "L0_CHANNEL_RE('Muon|DiMuon') | L0_CHANNEL( 'Hadron' )",

HLT_Code   = "(HLT_PASS_RE('Hlt1Track.*Decision') | HLT_PASS('Hlt1.*Muon.*Decision')) & HLT_PASS_RE('Hlt2.*Charm.*Decision')" 

)

#----------------------------------------------------------------

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

## #
## # Need to add bank-killer for testing
## from Configurables import EventNodeKiller
## eventNodeKiller = EventNodeKiller('Stripkiller')
## eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
## DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
## #

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
#DaVinci().UseTrigRawEvent=True


## # Bring in some local test data
## ## Signal test data
## ## Dst2DPi
## EventSelector().Input   = [

##  "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/WalterMCFiltering/00018228_00000002_1.dst_d0pi.strip.trig.dst' TYP='POOL_ROOTTREE' OPT='READ'"

##                           ]

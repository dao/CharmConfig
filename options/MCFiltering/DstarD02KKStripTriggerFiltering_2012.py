"""
Author: Kevin Maguire
Date: 23/11/2016
Options for building Stripping21 filtering on D2hhPromtDst2D2KKLine,
HLT1TrackL0 and HLT2CharmHadD0HH 
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
stripping='stripping21'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []
_charm = quickBuild('CharmToBeSwum')
MyLines = ['StrippingD2hhPromptDst2D2KKLine']

_charm.lines[:] = [ x for x in _charm.lines if x.name() in MyLines ]
for line in _charm.lines:
    line._prescale = 1.0
    print "charm has a line called " + line.name()
streams.append( _charm )

AllStreams = StrippingStream("PromptD2KK.StripTrig")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that we don't get all events written out

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )
#Add stripping TCK 
from Configurables import StrippingTCK
tck = 0x36132101
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=tck)

# Include trigger filtering
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code   = "(HLT_PASS_RE('Hlt1TrackAllL0.*Decision')) & HLT_PASS_RE('Hlt2CharmHadD02HH.*Decision') " 
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

#testing
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output-kk.txt"
DaVinci().MoniSequence += [ DumpFSR() ]
DaVinci().EvtMax = 30000           
DaVinci().DataType = "2012"
#DaVinci().DDDBtag    = 'MC11-20111102'
#DaVinci().CondDBtag  = "sim-20111111-vc-md100"
from GaudiConf import IOHelper
#IOHelper().inputFiles(['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00050282/0000/00050282_00000037_2.AllStreams.dst'], clear = True)
IOHelper().inputFiles(['/afs/cern.ch/work/k/kmaguire/MC/Brunel/Brunel-27163074-1000ev-20161114.dst'], clear = True)
DaVinci().DDDBtag   = "dddb-20130929-1"
DaVinci().CondDBtag = "sim-20130522-1-vc-md100"
DaVinci().DDDBtag   = "dddb-20130929-1"
DaVinci().CondDBtag = "sim-20130522-1-vc-md100"



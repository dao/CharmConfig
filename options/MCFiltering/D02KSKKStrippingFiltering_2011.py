"""
Options for building stripping20r1 with tight track chi2 cut (<3) and filtering on DstarD2KShhKK{LL,DD} decision
and Hlt2 D02HHKs{LL,DD} or Hlt2 inclusive D* decision
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20r1'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

# turn off all the PID cuts
hugenumber = 100000
myconfig = config['DstarD2KShh']
myconfig['CONFIG']['trackFromDCutPIDe'] = hugenumber # was 10.0
myconfig['CONFIG']['SoftPionCutPIDe'] = hugenumber # was 2.0
myconfig['CONFIG']['pionFromDCutPIDK'] = hugenumber # was -1.0
myconfig['CONFIG']['trackFromDCutPIDp'] = hugenumber # was 15.0
myconfig['CONFIG']['kaonFromDCutPIDpi'] = hugenumber # was -3.0

# 'config' is some read-only database handle, make a new dictionary to pass to buildStream()
newconfig = { 'DstarD2KShh' : myconfig }

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=newconfig, streamName=streamName, archive=archive)

streams = []

_charm         = quickBuild('CharmToBeSwum')

MyLines = ['StrippingDstarD2KShhKKLLLine']

_charm.lines[:] = [ x for x in _charm.lines if x.name() in MyLines ]
for line in _charm.lines:
    line._prescale = 1.0
    print "charm has a line called " + line.name()
streams.append( _charm )

AllStreams = StrippingStream("PromptD2KsKK.NoPIDStripTrig")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get all events written out

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Include trigger filtering
# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code   = "(HLT_PASS_RE('Hlt1TrackPhoton.*Decision') | HLT_PASS_RE('Hlt1TrackAllL0.*Decision')) & HLT_PASS_RE('Hlt2CharmHadD02HHKsLLDecision')" 
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

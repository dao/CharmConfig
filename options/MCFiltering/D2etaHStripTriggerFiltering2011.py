"""
Hlt Filtering for D->eta h
@author Simone Stracka
@date   2015-02-11

PAY ATTENTION: If your line is prescaled in the stripping you might want to adjust that!
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20r1p2'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = []

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)


_filterlines = quickBuild('Charm')

# Select lines by name
MyLines = [ 'StrippingD2PiPi0_eegammaKEtaPromptLine', 'StrippingD2PiPi0_eegammaKEtaPrimePromptLine', 'StrippingD2PiPi0_eegammaPiEtaPromptLine', 'StrippingD2PiPi0_eegammaPiEtaPrimePromptLine' ]

_filterlines.lines[:] = [ x for x in _filterlines.lines if x.name() in MyLines ]
for line in _filterlines.lines:
    line._prescale = 1.0
    print "charm has a line called " + line.name()
streams.append( _filterlines )

# Select lines you want
# Stream name will control name in book-keeping - make it something descriptive
AllStreams = StrippingStream("D2EtaH.StripTrig")

for stream in streams:
    AllStreams.appendLines(stream.lines)


# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents 
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

###########################################
###########################################

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )


#
# Configuration of SelDSTWriter
#
enablePacking = True

SelDSTWriterElements = {
    'default'              : stripDSTElements(pack=enablePacking)
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(pack=enablePacking)
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )


#----------Include trigger filtering---------------------------

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (

    L0DU_Code = "L0_CHANNEL('Electron')|L0_CHANNEL('Photon')|L0_CHANNEL('Hadron')|L0_CHANNEL('Muon')|L0_CHANNEL('DiMuon')|L0_CHANNEL('Muon,lowMult')|L0_CHANNEL('DiMuon,lowMult')|L0_CHANNEL('LocalPi0')|L0_CHANNEL('GlobalPi0')",
    
    HLT_Code = "HLT_PASS_RE('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision') & HLT_PASS_RE('Hlt2(?!Transparent).*Decision')"
    
    )

#----------------------------------------------------------------

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
#DaVinci().PrintFreq  = 200
DaVinci().EvtMax = -1 
DaVinci().HistogramFile = "DVHistos.root"

#
# Need to add bank-killer for testing
#from Configurables import EventNodeKiller
#eventNodeKiller = EventNodeKiller('Stripkiller')
#eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
# If you want to filter on trigger lines specified in trigfltrs above 
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

#Need the following lines for testing
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]


"""
Hlt Filtering file for DCS BR
@author Sandra Amato
@date   2012-11-21

PAY ATTENTION: If your line is prescaled in the stripping you might want to adjust that!
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

###########################################
###########################################


_filterlines = quickBuild('Charm')

# Select lines you want
# Stream name will control name in book-keeping - make it something descriptive
MyStream = StrippingStream("D2HHH.Hlt")
# Select lines by name
MyLines = ['StrippingD2hhh_KKKLine', 'StrippingD2hhh_KPPLine', 'StrippingD2hhh_KPPosLine', 'StrippingD2hhh_KKPosLine', 'StrippingD2hhh_KKPLine',  'StrippingD2hhh_HHHIncLine' ]


for line in _filterlines.lines :
    if line.name() in MyLines:
        MyStream.appendLines( [ line ] ) 

# Set prescales to 1.0 if necessary
for line in _filterlines.lines:
    line._prescale = 1.0
    
# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()


sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = True # Do not Filter the ones passing the stripping lines



###########################################
###########################################



from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )


#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )


#----------Include trigger filtering---------------------------

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (


      HLT_Code   = "HLT_PASS_RE('Hlt1TrackAllL0.*Decision') & HLT_PASS_RE('Hlt2CharmHadD2HHH.*Decision')" 

    )

#----------------------------------------------------------------

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
#DaVinci().PrintFreq  = 200
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

#
# Need to add bank-killer for testing
#from Configurables import EventNodeKiller
#eventNodeKiller = EventNodeKiller('Stripkiller')
#eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
#DaVinci().UseTrigRawEvent=True
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

## # Bring in some local test data
## ## Signal test data
## ## Dst2DPi
# EventSelector().Input   = [

##     "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/ChrisThomasMCFiltering/DstDPi/00017333_00000001_1.allstreams.dst ' TYP='POOL_ROOTTREE' OPT='READ'"
## #   ,"DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/ChrisThomasMCFiltering/DstDPi/00017333_00000002_1.allstreams.dst ' TYP='POOL_ROOTTREE' OPT='READ'"
#                           ]
#from GaudiConf import IOHelper
#IOHelper().inputFiles([
##MC/MC11a/21263020 ( D+_pi-pi+pi+=res,DecProdCut )
#"DATAFILE='PFN:/castor/cern.ch/grid/lhcb/MC/MC11a/ALLSTREAMS.DST/00013797/0000/00013797_00000009_1.allstreams.dst",
#"DATAFILE='PFN:/castor/cern.ch/grid/lhcb/MC/MC11a/ALLSTREAMS.DST/00013797/0000/00013797_00000057_1.allstreams.dst"
#evt://MC/MC11a/21263002 ( D+_K-K+pi+=res,DecProdCut )/Beam3500GeV-2011-MagDown-Nu2-EmNoCuts/Sim05/Trig0x40760037Flagged/Reco12a/Stripping17NoPrescalingFlagged/ALLSTREAMS.DST
#"DATAFILE='PFN:/castor/cern.ch/grid/lhcb/MC/MC11a/ALLSTREAMS.DST/00014698/0000/00014698_00000003_1.allstreams.dst"
#//MC/MC11a/21263010 ( D+_K-pi+pi+=res,DecProdCut )/Beam3500GeV-2011-MagDown-Nu2-EmNoCuts/Sim05/Trig0x40760037Flagged/Reco12a/Stripping17NoPrescalingFlagged/ALLSTREAMS.DST
#"DATAFILE='PFN:/castor/cern.ch/grid/lhcb/MC/MC11a/ALLSTREAMS.DST/00015505/0000/00015505_00000144_1.allstreams.dst"
#])

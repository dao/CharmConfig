"""
Hlt Filtering for D->hhh
@author Sandra Amato
@date   2014-07-11

PAY ATTENTION: If your line is prescaled in the stripping you might want to adjust that!
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []


_filterlines = quickBuild('Charm')

# Select lines you want
# Stream name will control name in book-keeping - make it something descriptive
MyStream = StrippingStream("D2HHH.HLT")
# Select lines by name
MyLines = ['StrippingD2hhh_PPPLine', 'StrippingD2hhh_HHHIncLine', 'StrippingD2hhh_KPPLine', 'StrippingD2hhh_KKPLine', 'StrippingD2hhh_KKKLine', 'StrippingD2hhh_KPPosLine', 'StrippingD2hhh_KKPosLine' ]

for line in _filterlines.lines :
    if line.name() in MyLines:
        MyStream.appendLines( [ line ] ) 

# Set prescales to 1.0 if necessary
for line in _filterlines.lines:
    line._prescale = 1.0
    
# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = True # Do not Filter the ones passing the stripping lines



from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )


#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )


#----------Include trigger filtering---------------------------

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (


      HLT_Code   = "HLT_PASS_RE('Hlt1TrackAllL0.*Decision') & HLT_PASS_RE('Hlt2CharmHadD2HHH.*Decision')" 

    )

#----------------------------------------------------------------

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().Simulation = True
#DaVinci().PrintFreq  = 200
DaVinci().EvtMax = -1 
DaVinci().HistogramFile = "DVHistos.root"

#
# Need to add bank-killer for testing
#from Configurables import EventNodeKiller
#eventNodeKiller = EventNodeKiller('Stripkiller')
#eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
# If you want to filter on trigger lines specified in trigfltrs above 
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

#Need the following lines for testing
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

#from GaudiConf import IOHelper
#IOHelper().inputFiles([
##MC2012 Kpipi php DecProd
#'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00035730/0000/00035730_00000006_1.allstreams.dst',
#'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00035730/0000/00035730_00000008_1.allstreams.dst',
#'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00035730/0000/00035730_00000009_1.allstreams.dst',
#'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00035730/0000/00035730_00000010_1.allstreams.dst',
#])

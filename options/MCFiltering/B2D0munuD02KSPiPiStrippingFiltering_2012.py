"""
Stripping Filtering for Strippingb2D0MuXKsPiPiLLCharmFromBSemiLine and Strippingb2D0MuXKsPiPiDDCharmFromBSemiLine
@author Stefanie Reichert
@date   2014-09-18
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
  "CloneDistCut" : [5000, 9e+99 ] }
#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20r0p1'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_filterlines = quickBuild('Charm')

# Stream name will control name in book-keeping - make it something descriptive
MyStream = StrippingStream("D02KSPiPi.Strip")
# Select lines by name
MyLines = [ 'Strippingb2D0MuXKsPiPiLLCharmFromBSemiLine', 'Strippingb2D0MuXKsPiPiDDCharmFromBSemiLine' ]

for line in _filterlines.lines :
  line._prescale = 1.0
  if line.name() in MyLines:
    MyStream.appendLines( [ line ] ) 
    
# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    AcceptBadEvents = False,
                    TESPrefix = 'Strip',
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )


#
# Configuration of SelDSTWriter
#
enablePacking = True
SelDSTWriterElements = {
    'default'              : stripDSTElements(pack=enablePacking)
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(pack=enablePacking)
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# DaVinci Configuration
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().Simulation = True
#DaVinci().PrintFreq  = 200
DaVinci().EvtMax = -1 
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

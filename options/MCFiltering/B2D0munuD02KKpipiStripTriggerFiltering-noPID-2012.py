"""
Stripping and trigger filtering file for semileptonic D0->hhhh with 2012 trigger line.
Uses modified stripping line with PID cuts removed.
@author Maxime Schubiger
@date 2017-03-29
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"



confdict = {
    "GEC_nLongTrk" : 250 # adimensional
    ,"TRGHOSTPROB" : 0.5 # adimensional
    ,"MuonGHOSTPROB" : 0.5 #adimensional
    ,"PrescalD0Mu"    : 0.5  # for D0->KPi line
    ,"PrescalDsPi_fakes" : 0.5  # for Bs->(Ds->PhiPi)Pi for Fakes line
    ,"MINIPCHI2"     : 9.0    # adimensiional
    ,"TRCHI2"        : 4.0    # adimensiional
    ,"TRCHI2Loose"   : 5.0    # adimensiional    
    ,"MuonIPCHI2"    : 4.00   # adimensiional
    ,"MuonPT"        : 800.0  # MeV
    ,"KPiPT"         : 300.0  # MeV
    ,"DsDIRA"        : 0.99   # adimensiional
    ,"DsFDCHI2"      : 100.0  # adimensiional
    ,"DsMassWin"     : 80.0   # MeV
    ,"DsAMassWin"    : 100.0  # MeV
    ,"D02HHHHMassWin": 60.0   # MeV
    ,"D02HHHHSumPT"  : 1800.0   # MeV
    ,"DsIP"          : 7.4    #mm
    ,"DsVCHI2DOF"    : 6.0    # adimensiional
    ,"BDIRA"         : 0.999  #adimensiional
    ,"BVCHI2DOF"     : 6.0    # adimensiional
    ,"DZLoose"       : -9999  #mm
    ,"DZ"            : 0  #mm
    ,"DDocaChi2Max"  : 20     #adimensiional
    ,"DDocaChi2MaxTight"  : 9.0     #adimensiional
    ,"MINIPCHI2Loose" : 4.0   #adimensiional
    ,"PhiVCHI2"      :25.0    #adimensiional
    ,"PhiMassWin"    :40      #adimensiional
    ,"Dstar_Chi2"       :  8.0 ## unitless
    ,"Dstar_SoftPion_PT" : 180. ## MeV ###
    ,"Dstar_wideDMCutLower" : 0. ## MeV
    ,"Dstar_wideDMCutUpper" : 170. ## MeV
    }
        
        
        
from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseKaons, StdLooseProtons, StdNoPIDsPions, StdNoPIDsKaons, StdLooseMergedPi0,StdLooseResolvedPi0
from Configurables import ConjugateNeutralPID






class B2DMuNuXAllLinesConf_NoPID(LineBuilder) :
    """
    """
    
    __configuration_keys__ = (
        "GEC_nLongTrk"
        ,"TRGHOSTPROB"
        ,"MuonGHOSTPROB"
        ,"PrescalD0Mu"
        ,"PrescalDsPi_fakes" 
        ,"MINIPCHI2"     
        ,"TRCHI2"     
        ,"TRCHI2Loose"   
        ,"MuonIPCHI2"    
        ,"MuonPT"        
        ,"KPiPT"               
        ,"DsDIRA"        
        ,"DsFDCHI2"      
        ,"DsMassWin"     
        ,"DsAMassWin"    
        ,"D02HHHHMassWin"
        ,"D02HHHHSumPT" 
        ,"DsIP"          
        ,"DsVCHI2DOF"    
        ,"BDIRA"         
        ,"BVCHI2DOF"     
        ,"DZ"
        ,"DZLoose"
        ,"DDocaChi2Max"
        ,"DDocaChi2MaxTight"
        ,"MINIPCHI2Loose"
        ,"PhiVCHI2"
        ,"PhiMassWin"
        ,"Dstar_Chi2"       
        ,"Dstar_SoftPion_PT" 
        ,"Dstar_wideDMCutLower" 
        ,"Dstar_wideDMCutUpper" 
        )
    
    __confdict__={}
        
    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.__confdict__=config

        ############### MUON SELECTIONS ###################
        self.selmuon = Selection( "Mufor" + name,
                                  Algorithm = self._muonFilter(),
                                  RequiredSelections = [StdLooseMuons])

        self.selmuonhighPT = Selection( "MuhighPTfor" + name,
                                  Algorithm = FilterDesktop( Code = "(TRCHI2DOF < %(TRCHI2)s) & (PT>1.2*GeV) & (MIPCHI2DV(PRIMARY)> 9.0)" % self.__confdict__ ),
                                  RequiredSelections = [self.selmuon])

        ############### KAON AND PION SELECTIONS ################
        
        self.selKaon = Selection( "Kfor" + name,
                                  Algorithm = self._kaonFilter(),
                                  #RequiredSelections = [StdLooseKaons])
                                  RequiredSelections = [StdNoPIDsKaons])
        
        self.selPion = Selection( "Pifor" + name,
                                  Algorithm = self._pionFilter(),
                                  RequiredSelections = [StdLoosePions])
        
        ################ D0 -> 4H SELECTION ##########################

        self.sel_D0_to_2K2Pi = Selection( "D022K2Pifor"+name,
                                         Algorithm = self._D02HHHHFilter([ 'D0 -> K- K+ pi- pi+' ]),
                                         RequiredSelections = [self.selKaon, self.selPion])
        
        ################ MAKE THE B CANDIDATES #################################
        
        self.selb2D0MuX2K2Pi = makeb2DMuX('b2D0MuX2K2Pi' + name,
                                         DecayDescriptors = [ '[B- -> D0 mu-]cc', '[B+ -> D0 mu+]cc' ],
                                         MuSel = self.selmuonhighPT, 
                                         DSel = self.sel_D0_to_2K2Pi,
                                         BVCHI2DOF = config['BVCHI2DOF'],
                                         BDIRA = config['BDIRA'],
                                         DZ = config['DZ']
                                         )        
        
        ################# DECLARE THE STRIPPING LINES #################################

        GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                 "Preambulo": ["from LoKiTracks.decorators import *"]}

        ########### D0 -> HHHHH
        self.b2D0MuX2K2PiLine = StrippingLine('b2D0MuX2K2Pi' + name + 'Line', prescale = 1, FILTER=GECs, selection = self.selb2D0MuX2K2Pi)
        
        ############## REGISTER THE LINES #####################

        self.registerLine(self.b2D0MuX2K2PiLine)


    def _muonFilter( self ):
        _code = "(PT > %(MuonPT)s *MeV) & (P> 3.0*GeV)"\
                "&(TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
                "& (TRCHI2DOF< %(TRCHI2Loose)s) & (MIPCHI2DV(PRIMARY)> %(MuonIPCHI2)s)" % self.__confdict__
        _mu = FilterDesktop( Code = _code )
        return _mu        

    def _pionFilter( self ):
        _code = "  (TRCHI2DOF < %(TRCHI2)s) & (P>2.0*GeV) & (PT > %(KPiPT)s *MeV)"\
                "&(TRGHOSTPROB < %(TRGHOSTPROB)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)" % self.__confdict__
        _pi = FilterDesktop( Code = _code )
        return _pi

    def _kaonFilter( self ):
        _code = "  (TRCHI2DOF < %(TRCHI2)s) & (P>2.0*GeV) & (PT > %(KPiPT)s *MeV)"\
                "&(TRGHOSTPROB < %(TRGHOSTPROB)s)"\
                "& (MIPCHI2DV(PRIMARY)> %(MINIPCHI2)s)" % self.__confdict__
        _ka = FilterDesktop( Code = _code )
        return _ka 

    def _D02HHHHFilter( self , _decayDescriptors):
        ### 5 MeV wider mass window for combination
        _combinationCut = "(ADAMASS('D0')-5.0 < %(D02HHHHMassWin)s *MeV)"\
                          "& (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) > %(D02HHHHSumPT)s *MeV)"\
                          "& (ADOCACHI2CUT( %(DDocaChi2MaxTight)s, ''))" % self.__confdict__
        _motherCut = " (ADMASS('D0') < %(D02HHHHMassWin)s *MeV)"\
                     " & (VFASPF(VCHI2/VDOF) < %(DsVCHI2DOF)s) " \
                     " & (SUMTREE( PT,  ISBASIC )> %(D02HHHHSumPT)s *MeV)"\
                     " & (BPVVDCHI2 > %(DsFDCHI2)s) &  (BPVDIRA> %(DsDIRA)s)"  % self.__confdict__
        _d02k3pi = CombineParticles( DecayDescriptors = _decayDescriptors,
                                     CombinationCut = _combinationCut,
                                     MotherCut = _motherCut)                            
        return _d02k3pi       
        
        

def makeb2DMuX(name,
               DecayDescriptors,
               MuSel,
               DSel,
               BVCHI2DOF,
               BDIRA,
               DZ):
    _combinationCut = "(AM<6.2*GeV)"
    _motherCut = "  (MM<6.0*GeV) & (MM>2.5*GeV) & (VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)  " \
                   "& (MINTREE(((ABSID=='D+') | (ABSID=='D0') | (ABSID=='Lambda_c+')) , VFASPF(VZ))-VFASPF(VZ) > %(DZ)s *mm ) "  % locals()
    #        _B.ReFitPVs = True
    _B = CombineParticles(DecayDescriptors = DecayDescriptors,
                          CombinationCut = _combinationCut,
                          MotherCut = _motherCut)
                          
    return Selection (name,
                      Algorithm = _B,
                      RequiredSelections = [MuSel, DSel])





                                
                                
                                

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream

AllStreams = StrippingStream("D02KKpipi.StripTrig") 

## Add modified line
myBuilder = B2DMuNuXAllLinesConf_NoPID('B2DMuNuXNoPID', confdict)
AllStreams.appendLines( myBuilder.lines() )

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False














#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = 'Filtered',
                          SelectionSequences = sc.activeStreams()
                          )





                          
                          
                          
                          
                                    











# Include trigger filtering
# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code = "(HLT_PASS_RE('Hlt1TrackAllL0.*Decision') | HLT_PASS_RE('Hlt1TrackAllMuon.*Decision')) & (HLT_PASS_RE('Hlt2SingleMuon.*Decision') | HLT_PASS_RE('Hlt2TopoMu2BodyBBDT.*Decision') | HLT_PASS_RE('Hlt2TopoMu3BodyBBDT.*Decision') | HLT_PASS_RE('Hlt2TopoMu4BodyBBDT.*Decision'))"
    )



# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x32212000)

# Track chi2 cut <4
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 4 ],
                                "CloneDistCut" : [5000, 9e+99 ] } 
           


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().DataType = "2012"
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 100
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().ProductionType = "Stripping"
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().HistogramFile = "DVHistos.root"

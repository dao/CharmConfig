"""
Stripping filtering file for Monte Carlo for D2Klnu Charm LNU analysis
2015,2016 data
@author A. Davis, modified from S. Ely
@date   2017-11-28
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"



#
# Hack. Copy the stripping line here without the PID
# BEGIN Stripping D2HMuNu.py
from StrippingUtils.Utils import LineBuilder
def TOSFilter( name, _input, _specs ) :
    from Configurables import TisTosParticleTagger
    from PhysSelPython.Wrappers import Selection 
    _tisTosFilter = TisTosParticleTagger( name + "Tagger" )
    _tisTosFilter.TisTosSpecs = _specs 
    return Selection( name
                      , Algorithm = _tisTosFilter
                      , RequiredSelections = [ _input ]
                      )        


__all__ = ('D2HLepNuBuilder',
           'TOSFilter',
           'default_config')

default_config = {
    'NAME'        : 'D2HMuNu',
    'WGs'         : ['Charm'],
    'BUILDERTYPE' : 'D2HLepNuBuilder',
    'CONFIG'      :  {
        "KLepMassLow"     : 500 , #MeV
        "KLepMassHigh"    : 2000 , #MeV
        "DELTA_MASS_MAX"  : 400  , #MeV
        "GEC_nLongTrk"    : 160. , #adimensional
        "TRGHOSTPROB"     : 0.35 ,#adimensional
        #Muons
        "MuonGHOSTPROB"   : 0.35 ,#adimensional
        #   "MuonP"               : 3000. ,#MeV
        "MuonPT"          : 500. ,#MeV
        "MuonPIDK"        : -1000.   ,#adimensional
        "MuonPIDmu"       : -1113.   ,#adimensional
        "MuonPIDp"        : -1110.   ,#adimensional
        "ElectronPIDe"    : -1110.0  ,
        "ElectronPT"      : 500  ,#MeV
        #Xu
        #K channel
        "KaonP"           : 3000.,#MeV
        "KaonPT"          : 800. ,#MeV
        "KaonPIDK"        : -1115.   ,#adimensional 
        "KaonPIDmu"       : -1115.   ,#adimensional
        "KaonPIDp"        : -1115.   ,#adimensional
        "KaonMINIPCHI2"   : 9    ,#adimensional
        "BVCHI2DOF"       : 20   ,#adminensional
        "BDIRA"           : 0.999,#adminensional
        "BFDCHI2HIGH"     : 100. ,#adimensional
        "BPVVDZcut"       : 0.0  , #mm
        #slow pion
        "Slowpion_PT"     : 300 #MeV
        ,"Slowpion_P"     : 1000 #MeV
        ,"Slowpion_TRGHOSTPROB" : 0.35 #adimensional
        ,"Slowpion_PIDe" : 1115 #adimensional
        ,"useTOS" : True #adimensional
        ,"TOSFilter" : { 'Hlt2CharmHad.*HHX.*Decision%TOS' : 0}  #adimensional
        ,"SSonly" : 2 # swith: 0==signal only (h-l+), 1==same sign hl, 2==both, 3 == 2016 EOY restripping
        },
    'STREAMS'     : ['CharmCompleteEvent']    
}


class D2HLepNuBuilder(LineBuilder):
    """
    Definition of D* tagged D0 -> H- mu+(e+) nu stripping
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self,name,config):
        LineBuilder.__init__(self, name, config)
        self._config=config
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        
        self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        self._muonSel=None
        self._muonFilter()
        
        self._electronSel=None
        self._electronFilter()
        
        self._slowpionSel =None
        self._pionSel=None
        self._kaonSel=None
        self._kaonFilter()
        self._pionFilter()
        self._slowpionFilter()
        
        ## OPPOSITE SIGN hl (BACKGROUND)
        self.registerLine(self._D2PiMuNuLine())
        self.registerLine(self._D2KMuNuLine())
        self.registerLine(self._D2PiENuLine())
        self.registerLine(self._D2KENuLine())
        
        ## SAME SIGN hl (BACKGROUND)
        self.registerLine(self._D2PiMuNuSSLine())
        self.registerLine(self._D2KMuNuSSLine())
        self.registerLine(self._D2PiENuSSLine())
        self.registerLine(self._D2KENuSSLine())

        #run the electron line too
    def _NominalMuSelection( self ):
        #        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV) &  (PT> %(MuonPT)s* MeV)"\
        return " (PT> %(MuonPT)s* MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"
               

    def _NominalKSelection( self ):
        #        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonPTight)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
        return "  (PT> %(KaonPT)s *MeV)"\
               "& (P> %(KaonP)s)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    def _NominalPiSelection( self ):
        #        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
        return " (PT> %(KaonPT)s *MeV)"\
               "& (P> %(KaonP)s)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(KaonMINIPCHI2)s )"

    def _NominalSlowPiSelection( self ):
        #        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
        return " (PT> %(Slowpion_PT)s *MeV)"\
               "& (P > %(Slowpion_P)s)"\
               "& (TRGHOSTPROB < %(Slowpion_TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)< 9.0) "

    def _NominalElectronSelection( self ):
        return " (PT> %(ElectronPT)s *MeV) & (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               

    ######--######
    def _electronFilter( self ):
        if self._electronSel is not None:
            return self._electronSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllNoPIDsElectrons
        _el = FilterDesktop( Code = self._NominalElectronSelection() % self._config )
        _electronSel=Selection("Electron_for"+self._name,
                         Algorithm=_el,
                         RequiredSelections = [StdAllNoPIDsElectrons])
        
        self._electronSel=_electronSel
        
        return _electronSel

    ######--######
    def _muonFilter( self ):
        if self._muonSel is not None:
            return self._muonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllNoPIDsMuons
        _mu = FilterDesktop( Code = self._NominalMuSelection() % self._config )
        _muSel=Selection("Mu_for"+self._name,
                         Algorithm=_mu,
                         RequiredSelections = [StdAllNoPIDsMuons])
        
        self._muonSel=_muSel
        
        return _muSel

    def _pionFilter( self ):
        if self._pionSel is not None:
            return self._pionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllNoPIDsPions
        
        _ka = FilterDesktop( Code = self._NominalPiSelection() % self._config )
        _kaSel=Selection("Pi_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdAllNoPIDsPions])
        self._pionSel=_kaSel
        return _kaSel

    ######Kaon Filter######
    def _kaonFilter( self ):
        if self._kaonSel is not None:
            return self._kaonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllNoPIDsKaons
        
        _ka = FilterDesktop( Code = self._NominalKSelection() % self._config )
        _kaSel=Selection("K_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdAllNoPIDsKaons])
        self._kaonSel=_kaSel
        return _kaSel

    ######Slow Pion Filter######
    def _slowpionFilter( self ):
        if self._slowpionSel is not None:
            return self._slowpionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllNoPIDsPions
        
        _ka = FilterDesktop( Code = self._NominalSlowPiSelection() % self._config )
        _kaSel=Selection("pis_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdAllNoPIDsPions])
        self._slowpionSel=_kaSel
        return _kaSel

    ## OPPOSITE SIGN hl (SIGNAL)
    def _D2PiMuNuLine( self ):
        return self.DstarMaker("D2pimu", ["[D0 -> pi- mu+]cc"], self._pionFilter(), self._muonFilter())

    def _D2KMuNuLine( self ):
        return self.DstarMaker("D2Kmu",["[D0 -> K- mu+]cc"], self._kaonFilter(), self._muonFilter())

    def _D2PiENuLine( self ):
        return self.DstarMaker("D2piE", ["[D0 -> pi- e+]cc"], self._pionFilter(), self._electronFilter())

    def _D2KENuLine( self ):
        return self.DstarMaker("D2KE",["[D0 -> K- e+]cc"], self._kaonFilter(), self._electronFilter())

    ## SAME SIGN hl (BACKGROUND)
    def _D2PiMuNuSSLine( self ):
        return self.DstarMaker("D2pimuSS", ["[D0 -> pi- mu-]cc"], self._pionFilter(), self._muonFilter())

    def _D2KMuNuSSLine( self ):
        return self.DstarMaker("D2KmuSS",["[D0 -> K- mu-]cc"], self._kaonFilter(), self._muonFilter())

    def _D2PiENuSSLine( self ):
        return self.DstarMaker("D2piESS", ["[D0 -> pi- e-]cc"], self._pionFilter(), self._electronFilter())

    def _D2KENuSSLine( self ):
        return self.DstarMaker("D2KESS",["[D0 -> K- e-]cc"], self._kaonFilter(), self._electronFilter())

    def DstarMaker(self, _name, _D0Decays, hfiler, lfilter):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StrippingConf.StrippingLine import StrippingLine
        
        _KMu = CombineParticles(
            DecayDescriptors = _D0Decays,
            CombinationCut = "(AM>%(KLepMassLow)s*MeV) & (AM<%(KLepMassHigh)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s )"\
            "& (BPVVDCHI2 >%(BFDCHI2HIGH)s)"\
            "& (BPVDIRA > %(BDIRA)s)"\
            "& (BPVVDZ > %(BPVVDZcut)s)"
            #            "& (BPVVD > %(VDCut)s)"
            #" & (ratio > 0.0)"
            % self._config,
            ReFitPVs = True
            )
        
        _D0Sel=Selection("SelD0_for"+_name,
                         Algorithm=_KMu,
                         RequiredSelections = [lfilter, hfiler])


        DstComb = CombineParticles( #name = "CombDst"+_name,
                DecayDescriptors = ['[D*(2010)+ -> D0 pi+]cc'],
                CombinationCut = "(AM - ACHILD(M,1) < %(DELTA_MASS_MAX)s+5 *MeV) & (ADOCACHI2CUT(20,''))" %self._config,
                MotherCut = "(M - CHILD(M,1) < %(DELTA_MASS_MAX)s *MeV) & (VFASPF(VCHI2/VDOF) < 9.0)" %self._config
                )
        DstSel = Selection("SelDst"+_name,
                Algorithm = DstComb,
                RequiredSelections = [_D0Sel,self._slowpionFilter()])

        _tosFilter = self._config['TOSFilter']
        DstSelTOS = TOSFilter( "SelDstHMu_Hlt2TOS"+_name
                ,DstSel
                ,_tosFilter)
    
        hlt2 = ""        
        the_prescale = 1.0
        if "K" in _name:
            the_prescale = 1.
            if 'SS' in _name:
                the_prescale = 1.
            Line = StrippingLine(_name+'Line',
                    prescale = the_prescale,
                    FILTER=self.GECs,
                    HLT2 = hlt2,
                    selection = DstSelTOS) 
        else:
            if "K" in _name:
                the_prescale = 1.
            Line = StrippingLine(_name+'Line',
                    prescale = the_prescale,
                    FILTER=self.GECs,
                    HLT2 = hlt2,
                    selection = DstSel) 
        
        return Line
##### end StrippingD2HMuNu without PID                                                    
#stripping version
stripping='stripping28'

#apply the thing above here.
from StrippingConf.Configuration import StrippingConf, StrippingStream,StrippingLine
confD2HLepNu = D2HLepNuBuilder(name = default_config["NAME"]+'_NOPID',config = default_config['CONFIG']);
streams = default_config['STREAMS']
lines = []
for x in streams:
    ###some problem here
    for line in confD2HLepNu.lines():
        lines.append(line)
    
for l in lines:
    print "added line "+l.name()

from StrippingSettings.Utils import strippingConfiguration

MyStream = StrippingStream("d2HMuNuStream.Strip")
MyLines = []
#for stream in streams:
for line in lines:
    line._prescale = 1.0
    MyLines.append(line)

MyStream.appendLines(MyLines)
    
#
# Remove All prescalings
# Merge into one stream and run in flag mode
#
            
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [MyStream],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents,
                    TESPrefix = 'Strip'
                    )
            
MyStream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out

#
# Configuration of SelDSTWriter
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }
            
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = '000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports',TCK=0x42612800)

#
# DaVinci Configuration
#
from Configurables import DaVinci, DumpFSR

dv = DaVinci()
fsr = DumpFSR()
fsr.OutputLevel = 3
fsr.AsciiFileName = "dumpfsr_check_output2016.txt"

dv.InputType = 'DST'
#dv.DataType = "2016"
dv.EvtMax = -1
dv.Simulation = True
dv.Lumi = False
#dv.DDDBtag   = "dddb-20150724"
#dv.CondDBtag = "sim-20160907-vc-md100"
dv.HistogramFile = "DVHistos.root"
dv.appendToMainSequence([ sc.sequence() ])
dv.appendToMainSequence([ stck ])
dv.appendToMainSequence([ dstWriter.sequence() ])
dv.MoniSequence += [DumpFSR()]
dv.ProductionType = "Stripping"

# Change the column size of Timing Table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr_check_output.txt"

# from GaudiConf import IOHelper
# IOHelper().inputFiles(["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST/00062304/0000/00062304_00000631_5.AllStreams.dst",
#                        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST/00062304/0000/00062304_00000002_5.AllStreams.dst",
#                        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST/00062304/0000/00062304_00000003_5.AllStreams.dst",
#                        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST/00062304/0000/00062304_00000005_5.AllStreams.dst",
#                        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST/00062304/0000/00062304_00000006_5.AllStreams.dst"],clear=True);

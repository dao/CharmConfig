""" 
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamKp_LamLLTurbo 
@author Yuehong Xie; Xiaopan Yang; Guiyun Ding
@date 2017-06-23
"""

from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence 
the_line = AutomaticData( '/Event/Turbo/Hlt2CharmHadLcp2LamKp_LamLLTurbo/Particles')
line = 'Hlt2CharmHadLcp2LamKp_LamLLTurboDecision'
selseq   = SelectionSequence( 'SEQ' , the_line ) 

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

#
# Configuration of SelDSTWriter
#
enablePacking = True

SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking)
    }


SelDSTWriterConf = {
    'default': stripDSTStreamConf(pack=enablePacking)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

#turbo_filter = GaudiSequencer('TurboFilter',
#                              RequireObjects=[line],
#                              Members=[dstWriter.sequence()])

#from PhysConf.Selections import AutomaticData, SelectionSequence
dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Lc2pkpi_LL',
                         SelectionSequences= [ selseq ]                       
                         )
# Include trigger filtering
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code   = "(HLT_PASS_RE('"+line+"')) " 
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
#DaVinci().appendToMainSequence([turbo_filter])

"""
from GaudiConf import IOHelper
IOHelper().inputFiles([
   '/eos/lhcb/user/x/xiaopan/Root/Lc2pkpi/16_MC/FCN/MagDown/00057229_00000033_3.AllStreams.dst'
  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2pkpi/16_MC/FCN/MagDown/00057229_00000035_3.AllStreams.dst'
  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2pkpi/16_MC/FCN/MagDown/00057229_00000048_3.AllStreams.dst'
  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2pkpi/16_MC/FCN/MagDown/00057229_00000079_3.AllStreams.dst'
  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2pkpi/16_MC/FCN/MagUp/00057227_00000031_3.AllStreams.dst'
  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2pkpi/16_MC/FCN/MagUp/00057227_00000037_3.AllStreams.dst'
  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2pkpi/16_MC/FCN/MagUp/00057227_00000080_3.AllStreams.dst'
  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2pkpi/16_MC/FCN/MagUp/00057227_00000099_3.AllStreams.dst'
 ], clear=True)

"""

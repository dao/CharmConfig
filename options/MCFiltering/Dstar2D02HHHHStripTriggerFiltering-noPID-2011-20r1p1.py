"""
Stripping and trigger filtering file for D0->KKKpi with 2011 trigger line.
Uses regular stripping line with PID cuts removed.
@author Tom Hampson
@date 2013-11-19
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Track chi2 cut <4
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 4 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20r1p1'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

# turn off all the PID cuts
myconfig = config['DstarPromptWithD02HHHH']
myconfig['CONFIG']['ApplyKaonPIDK'] = False
myconfig['CONFIG']['KaonPIDK'] = -100
myconfig['CONFIG']['ApplyPionPIDK'] = False
myconfig['CONFIG']['PionPIDK'] = 100

# 'config' is some read-only database handle, make a new dictionary to pass to buildStream()
newconfig = { 'DstarPromptWithD02HHHH' : myconfig }

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=newconfig, streamName=streamName, archive=archive)

streams = []

_charm         = quickBuild('Charm')

MyLines = ['StrippingDstarPromptWithD02HHHHLine']

_charm.lines[:] = [ x for x in _charm.lines if x.name() in MyLines ]
for line in _charm.lines:
    line._prescale = 1.0
    print "charm has a line called " + line.name()
streams.append( _charm )

AllStreams = StrippingStream("D02HHHH.StripTrig")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Include trigger filtering
# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code   = "HLT_PASS_RE('Hlt2CharmHadD02HHHHDecision')" 
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().DataType = "2011"
DaVinci().EvtMax = -1                       # Number of events
DaVinci().PrintFreq = 1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

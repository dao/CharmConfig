""" 
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamPip_LamDDTurbo
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamPip_LamLLTurbo
@author Yuehong Xie; Xiaopan Yang; Guiyun Ding
@date 2017-10-08
"""

from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, MultiSelectionSequence, MomentumScaling 

line1name = 'Hlt2CharmHadLcp2LamPip_LamDDTurbo'
line2name = 'Hlt2CharmHadLcp2LamPip_LamLLTurbo'

line1 = AutomaticData( '/Event/Turbo/'+line1name+'/Particles')
line2 = AutomaticData( '/Event/Turbo/'+line2name+'/Particles')
      
sel1   = SelectionSequence( 'SEQ1' , line1) 
sel2   = SelectionSequence( 'SEQ2' , line2) 
selseq = MultiSelectionSequence('MULTI', Sequences = [sel1, sel2])

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
#
# Configuration of SelDSTWriter
#
enablePacking = True

SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default': stripDSTStreamConf(pack=enablePacking)
    }

dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Lc2ppipi',
                         SelectionSequences= [ selseq ]                       
                         )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

#from GaudiConf import IOHelper
#IOHelper().inputFiles([
#   '/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000012_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000022_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000042_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000076_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000085_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000111_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000120_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000128_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000148_3.AllStreams.dst'
#  ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/v3_FCN/MagDown/00057231_00000177_3.AllStreams.dst'
#  ], clear=True)


"""
@author Andrea Contu
@date   2016-02-08

PAY ATTENTION: If your line is prescaled in the stripping you might want to adjust that!
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%40W%S%20W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

#
# Merge into one stream and run in filtering mode
#
MyStream = StrippingStream("Dst2D0PiD02mumu.Strip")

# Select lines by name
MyLines = [ 'StrippingDstarD02xxDst2PiD02mumuBox' ]

# Remove HLT filtering and append the line to the stream
for stream in streams:
    if 'Charm' in stream.name():
        for line in stream.lines:
            if line.name() in MyLines:
                line._HLT=None
                MyStream.appendLines( [ line ] ) 

    
# Configure Stripping
sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip',
                     )

MyStream.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

###########################################
###########################################



from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )


#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x32212000)


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )


"""
Stripping Filtering file for Chris Thomas
@author Sam Gregson
@date   2012-06-01

PAY ATTENTION: If your line is prescaled in the stripping you might want to adjust that!
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

###########################################
###########################################

# Lines demanded by analyst (Chris Thomas)
#StrippingDstarForPromptCharm and StrippingD02HHForPromptCharm 

_filterlines = quickBuild('Charm')

# Select lines you want
# Stream name will control name in book-keeping - make it something descriptive
MyStream = StrippingStream("PromptDACP.StripTrig")
# Select lines by name
MyLines = [ 'StrippingD02HHForPromptCharm', 'StrippingDstarForPromptCharm' ]

for line in _filterlines.lines :
    if line.name() in MyLines:
        MyStream.appendLines( [ line ] ) 

# Set prescales to 1.0 if necessary
for line in _filterlines.lines:
    line._prescale = 1.0
    
# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

###########################################
###########################################



from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )


#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )


#----------Include trigger filtering---------------------------

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (


      HLT_Code   = "HLT_PASS_RE('Hlt1Track.*Decision') & HLT_PASS_RE('Hlt2.*Charm.*Decision')" 

    )

#----------------------------------------------------------------

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

#
# Need to add bank-killer for testing
#from Configurables import EventNodeKiller
#eventNodeKiller = EventNodeKiller('Stripkiller')
#eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
#DaVinci().UseTrigRawEvent=True


## # Bring in some local test data
## ## Signal test data
## ## Dst2DPi
## EventSelector().Input   = [

##     "DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/ChrisThomasMCFiltering/DstDPi/00017333_00000001_1.allstreams.dst ' TYP='POOL_ROOTTREE' OPT='READ'"
## #   ,"DATAFILE='PFN:/var/pcjm/r02/lhcb/gregson/gangadir/testdata/ChrisThomasMCFiltering/DstDPi/00017333_00000002_1.allstreams.dst ' TYP='POOL_ROOTTREE' OPT='READ'"

##                           ]

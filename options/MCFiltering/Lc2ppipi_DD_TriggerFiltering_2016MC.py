""" 
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamPip_LamDDTurbo
@author Yuehong Xie; Xiaopan Yang; Guiyun Ding
@date 2017-06-23
"""

from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence 
the_line = AutomaticData( '/Event/Turbo/Hlt2CharmHadLcp2LamPip_LamDDTurbo/Particles')
line = 'Hlt2CharmHadLcp2LamPip_LamDDTurboDecision'
selseq   = SelectionSequence( 'SEQ' , the_line ) 

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

#
# Configuration of SelDSTWriter
#
enablePacking = True

SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking)
    }


SelDSTWriterConf = {
    'default': stripDSTStreamConf(pack=enablePacking)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

#turbo_filter = GaudiSequencer('TurboFilter',
#                              RequireObjects=[line],
#                              Members=[dstWriter.sequence()])

#from PhysConf.Selections import AutomaticData, SelectionSequence
dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Lc2ppipi_DD',
                         SelectionSequences= [ selseq ]                       
                         )
# Include trigger filtering
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code   = "(HLT_PASS_RE('"+line+"')) " 
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
#DaVinci().appendToMainSequence([turbo_filter])

"""
from GaudiConf import IOHelper
IOHelper().inputFiles([
#  '/afs/cern.ch/work/x/xiaopan/work/FCN/2016_MC/00057225_00000218_3.AllStreams.dst'
   '/afs/cern.ch/work/x/xiaopan/work/FCN/2016_MC/00057225_00000186_3.AllStreams.dst'
   ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/MagDown/00057231_00000022_3.AllStreams.dst'
   ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/MagDown/00057231_00000042_3.AllStreams.dst'
   ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/MagDown/00057231_00000064_3.AllStreams.dst'
   ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/MagUp/00057231_00000103_3.AllStreams.dst'
   ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/MagUp/00057231_00000110_3.AllStreams.dst'
   ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/MagUp/00057231_00000120_3.AllStreams.dst'
   ,'/eos/lhcb/user/x/xiaopan/Root/Lc2ppipi/16_MC/FCN/MagUp/00057231_00000141_3.AllStreams.dst'
  ], clear=True)
"""


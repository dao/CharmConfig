
from Gaudi.Configuration import *



from Configurables import DaVinci, SelDSTWriter, FilterDesktop, CombineParticles, OfflineVertexFitter

from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand, MergedSelection


from Configurables import FilterDesktop

name = "HMuNu"

from Configurables import TrackSmeared
TrackSmeared("TrackSmearing").smearBest = True
TrackSmearingSeq = GaudiSequencer("TrackSmearingSeq")
TrackSmearingSeq.Members = [ TrackSmeared("TrackSmearing") ]

from DecayTreeTuple.Configuration import *


_stdMuons = DataOnDemand(Location = "Phys/StdNoPIDsMuons/Particles")
_stdAllPions = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_stdPions = DataOnDemand(Location = "Phys/StdNoPIDsPions/Particles")
_stdKaons = DataOnDemand(Location = "Phys/StdNoPIDsKaons/Particles")


_muonFilter = FilterDesktop('muonFilter', 
                          Code = "(PT > 800)")
MuonFilterSel = Selection(name = 'MuonFilterSel',
                          Algorithm = _muonFilter,
                          RequiredSelections = [ _stdMuons ])

_pionFilter = FilterDesktop('pionFilter', 
                          Code = "(PT > 600)")
PionFilterSel = Selection(name = 'PionFilterSel',
                          Algorithm = _pionFilter,
                          RequiredSelections = [ _stdPions ])

_kaonFilter = FilterDesktop('kaonFilter', 
                          Code = "(PT > 600)")
KaonFilterSel = Selection(name = 'KaonFilterSel',
                          Algorithm = _kaonFilter,
                          RequiredSelections = [ _stdKaons ])



_DHMuNu = CombineParticles("D_" + name,
                            DecayDescriptors = ["[D0 -> mu+ pi-]cc","[D0 -> mu+ K-]cc"],
                            CombinationCut = "(AMAXDOCA('') < 0.07) & ((APT1 + APT2) > 2800 *MeV) & (AM < 1950 * MeV)",
                            MotherCut = "(VFASPF(VCHI2/VDOF)<9) & (BPVVD > 4 * mm) & (P > 20000 * MeV) & (BPVCORRM > 1400 * MeV) & (BPVCORRM < 2700 * MeV)")
DHMuNu = Selection ("SelD"+name,
                     Algorithm = _DHMuNu,
                     RequiredSelections = [MuonFilterSel, PionFilterSel,KaonFilterSel])

_DstD0Pi = CombineParticles("Dst_" + name,
                            DecayDescriptor = "[D*(2010)+ -> D0 pi+]cc",
                            CombinationCut = "(AMAXDOCA('') < 0.4 * mm)",
                            MotherCut = "(VFASPF(VCHI2/VDOF) < 9) & ( ( M - CHILD(M,1) ) < 200 * MeV)")
DstD0Pi = Selection ("SelDstD0"+name,
                     Algorithm = _DstD0Pi,
                     RequiredSelections = [DHMuNu, _stdAllPions])




seq = SelectionSequence("StripNoPID.CharmForVub", 
                          TopSelection = DstD0Pi)


##################

dstWriter = SelDSTWriter('MyDSTWriter',
                   SelectionSequences = [seq])

IODataManager().AgeLimit = 2


DaVinci().appendToMainSequence( [ TrackSmearingSeq ] )
DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

DaVinci().HistogramFile = "DVHistos.root"
DaVinci().EvtMax = -1
DaVinci().Simulation = True






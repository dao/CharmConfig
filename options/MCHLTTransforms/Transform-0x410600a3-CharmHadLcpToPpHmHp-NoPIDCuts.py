from Configurables import Moore
Moore().InitialTCK='0x410600a3'
from Configurables import HltConfigSvc
trans={
  "GaudiSequencer/Hlt2$" : {
    "Members" : { "^.*$" : """[ 'Hlt::Line/Hlt2CharmHadLcpToPpKmPipTurbo',
                                'Hlt::Line/Hlt2CharmHadLcpToPpKpPimTurbo',
                                'Hlt::Line/Hlt2CharmHadLcpToPpKmKpTurbo',
                                'Hlt::Line/Hlt2CharmHadLcpToPpPimPipTurbo',
                                'Hlt::Line/Hlt2ErrorEvent',
                                'Hlt::Line/Hlt2Global']"""},
  },
  ".*/Hlt2BiKalmanFittedRichProtons.Proton" : {
    "Selection" : { "^.*$" : """[""]"""},
  },
  ".*/Hlt2CharmHadSharedDetachedLcChild_pFilter" : {
    "Code" : { "^.*$" : """  (TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 4.0)& ( ALL )"""},
  },
  ".*/Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter" : {
    "Code" : { "^.*$" : """( ALL ) & ( ALL ) & ( P > 10000.0 )"""},
  },
  ".*/Hlt2BiKalmanFittedRichKaons.Kaon" : {
    "Selection" : { "^.*$" : """[""]"""},
  },
  ".*/Hlt2CharmHadSharedDetachedLcChild_KFilter" : {
    "Code" : { "^.*$" : """  (TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 4.0)& ( ALL )"""},
  },
  ".*/Hlt2BiKalmanFittedRichPions.Pion" : {
    "Selection" : { "^.*$" : """[""]"""},
  },
  ".*/Hlt2CharmHadSharedDetachedLcChild_piFilter" : {
    "Code" : { "^.*$" : """  (TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 4.0)& ( ALL )"""},
  },
}
HltConfigSvc().setProp("ApplyTransformation",trans)


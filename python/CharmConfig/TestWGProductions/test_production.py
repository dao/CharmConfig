#!/usr/bin/env python
from __future__ import print_function

import argparse
import os
from os.path import basename, dirname, isfile, join
import shutil
from subprocess import check_output
import sys
from tempfile import NamedTemporaryFile


dirac_config_source_fn = join(os.getcwd(), dirname(__file__), 'dirac.cfg')
dirac_config_dest_fn = join(os.environ['HOME'], '.dirac.cfg')


def get_lfns(bkpath):
    with NamedTemporaryFile(suffix='.lfns') as fp:
        command = 'dirac-bookkeeping-get-files --DQFlags=OK --BKQuery="{bkpath}" --Output={output_fn}'
        command = command.format(bkpath=bkpath, output_fn=fp.name)
        check_output(command, shell=True)
        with open(fp.name, 'rt') as fp:
            lfns = [s.split(' ')[0] for s in fp.readlines()]
        assert len(lfns) > 0, (command, bkpath)
    return lfns


def run_with_step(step_ids, bkpath, nlfns, configure_dirac=True):
    lfns = get_lfns(bkpath)[:nlfns]
    run_test_production(lfns, stepReady=True, stepsList=step_ids,
                        configure_dirac=configure_dirac)


def run_without_step(app, app_version, options, bkpath, nlfns, output_types,
                     extra_packages, configure_dirac=True):
    lfns = get_lfns(bkpath)[:nlfns]
    output_types = [o.upper() for o in output_types]

    # And, one more question: is this a merging step that you are testing?

    step = {
        'StepId': 12345, 'StepName': 'Whatever',
        'ApplicationName': app,
        'ApplicationVersion': app_version,
        'ExtraPackages': ';'.join(extra_packages),
        'ProcessingPass': 'whoCares',
        'Visible': 'N',
        'Usable': 'Yes',
        'DDDB': '',
        'CONDDB': '',
        'DQTag': '',
        'OptionsFormat': 'WGProd',
        'OptionFiles': ';'.join(options),
        'isMulticore': 'N',
        'SystemConfig': os.environ["CMTCONFIG"],
        'mcTCK': '',
        'ExtraOptions': '',
        'fileTypesIn': [basename(bkpath)],
        'fileTypesOut': output_types,
        'OutputFilePrefix': '',
        'visibilityFlag': [
            {'Visible': 'Y', 'FileType': output_types}
        ]
    }

    print('\n' + '*'*80 + '\n')
    print('Step is:', step)
    print('\n' + '*'*80 + '\n')

    run_test_production(lfns, fileTypesOut=output_types, steps=[step],
                        configure_dirac=configure_dirac)


def run_test_production(lfns, steps=None, fileTypesOut=None,
                        stepReady=False, stepsList=None, configure_dirac=True):
    if configure_dirac:
        assert not isfile(dirac_config_dest_fn), dirac_config_dest_fn
        shutil.copy(dirac_config_source_fn, dirac_config_dest_fn)

    from DIRAC.Core.Base.Script import parseCommandLine
    sys.argv = ['-ddd']
    parseCommandLine()

    from LHCbDIRAC.Interfaces.API.DiracProduction import DiracProduction
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequest import ProductionRequest

    # Options that I don't understand/support yet
    includeAncestors = False
    ancestorsDepth = 1
    mergingStep = False

    print('Using LFNs:', *lfns)

    pr = ProductionRequest()
    diracProduction = DiracProduction()

    if stepReady:
        pr.stepsList = stepsList
        pr.outputVisFlag = [{"1": 'Y'}]
        pr.resolveSteps()
        steps = pr.stepsListDict
        pr.outputSEs = ['Tier1-DST']
        pr.specialOutputSEs = [{}]
        pr._determineOutputSEs()
        outDict = pr.outputSEsPerFileType[0]
    else:
        pr.outputSEs = ['Tier1-DST']
        pr.specialOutputSEs = [{}]
        outDict = {t: 'Tier1-DST' for t in fileTypesOut}

    if not includeAncestors:
        ancestorsDepth = 0

    if mergingStep:
        jobType = 'Merge'
    else:
        jobType = 'Turbo'
    prod = pr._buildProduction(
        jobType, steps, outDict, 0, 100, inputDataPolicy='protocol',
        inputDataList=lfns, ancestorDepth=ancestorsDepth
    )

    diracProduction.launchProduction(prod, False, True, 0)

    if configure_dirac:
        os.remove(dirac_config_dest_fn)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test WG production')
    parser.add_argument('--nlfns', type=int, default=2)
    parser.add_argument('--bkpath', required=True)
    parser.add_argument('--app', default=None)
    parser.add_argument('--app-version', default=None)
    parser.add_argument('-o', '--options', action='append', default=None)
    parser.add_argument('--output-type', action='append', default=None)
    parser.add_argument('--extra-package', action='append', default=[])
    parser.add_argument('--no-dirac-config', action='store_true')
    parser.add_argument('--step-id', action='append', default=None)

    args = parser.parse_args()
    group = [
        args.app is not None,
        args.app_version is not None,
        args.options is not None,
        args.output_type is not None,
    ]
    print(group, args)
    assert (all(group) and not args.step_id) or (args.step_id and not any(group))

    if all(group):
        run_without_step(
            args.app, args.app_version, args.options, args.bkpath, args.nlfns,
            args.output_type, args.extra_package,
            configure_dirac=not args.no_dirac_config
        )
    else:
        steps = list(map(int, args.step_id))
        run_with_step(steps, args.bkpath, args.nlfns,
                      configure_dirac=not args.no_dirac_config)

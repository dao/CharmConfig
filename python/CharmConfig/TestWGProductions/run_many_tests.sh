#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t '

SCRIPT=$(readlink -f "$PWD/../../../python/CharmConfig/TestWGProductions/test_production.py")
export CHARMCONFIGROOT="$PWD/../../.."

# Check that we are running in the LHCbDIRAC environment
which dirac-bookkeeping-get-files

if [[ "$#" == 2 ]]; then
    TESTS_SCRIPT=$1
    USE_STEP=$2
    if [[ ("$USE_STEP" != "0") && ("$USE_STEP" != "1") ]]; then
        echo "USAGE: ./run_many_tests.sh tests_to_run.sh [USE_STEP]"
        echo
        echo "ERROR: USE_STEP must be 0 or 1"
        exit 1
    fi
elif [[ "$#" == 1 ]]; then
    TESTS_SCRIPT=$1
    USE_STEP="0"
else
    echo "USAGE: ./run_many_tests.sh tests_to_run.sh [USE_STEP]"
    exit 1
fi

function run_test {
    DIR=$1
    BKPATH_DOWN=$2
    BKPATH_UP=$3
    OPTIONS=$(for FN in $4; do echo "-o='$OPTIONS_DIR/$FN'"; done)
    OUTPUT=$5
    STEP_ID=$(for STEP in $6; do echo "--step-id=$STEP"; done)

    mkdir -p "${DIR}"

    (
        mkdir "${DIR}/MagDown"
        cd "${DIR}/MagDown"
        pwd
        if [[ "$USE_STEP" == "0" ]]; then
            $SCRIPT --no-dirac-config --nlfns=1 \
              --bkpath="${BKPATH_DOWN}" \
              --app DaVinci --app-version v42r6p1 \
              ${OPTIONS} \
              --output-type="${OUTPUT}"
        else
            $SCRIPT --no-dirac-config --nlfns=3 \
              --bkpath="${BKPATH_DOWN}" \
              ${STEP_ID}
        fi
    ) 2>&1 | tee "${DIR}/DIRAC_MagDown.log" > /dev/null 2>&1 &

    (
        mkdir "${DIR}/MagUp"
        cd "${DIR}/MagUp"
        pwd
        if [[ "$USE_STEP" == "0" ]]; then
            $SCRIPT --no-dirac-config --nlfns=3 \
              --bkpath="${BKPATH_UP}" \
              --app DaVinci --app-version v42r6p1 \
              ${OPTIONS} \
              --output-type="${OUTPUT}"
        else
            $SCRIPT --no-dirac-config --nlfns=3 \
              --bkpath="${BKPATH_UP}" \
              ${STEP_ID}
        fi
    ) 2>&1 | tee "${DIR}/DIRAC_MagUp.log" > /dev/null 2>&1 &
}

# Configure DIRAC
if [ -e "$HOME/.dirac.cfg" ]; then
  echo "ERROR: $HOME/.dirac.cfg exists"
  exit 1
fi
cp "$(dirname "$SCRIPT")/dirac.cfg" "$HOME/.dirac.cfg"

source "$TESTS_SCRIPT"

FAIL=0
for job in $(jobs -p); do
    echo "Wating for $job"
    wait "$job" || let "FAIL+=1"
done

# Clean up the DIRAC configuration
rm "$HOME/.dirac.cfg"

if [ "$FAIL" == "0" ];
then
echo "All jobs succeeded"
else
echo "$FAIL jobs failed"
fi

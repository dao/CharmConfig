This is a script for testing production steps prior to submission. Full instructions can be found here:

    https://twiki.cern.ch/twiki/bin/view/LHCb/ProdReqPreLaunchTest

To run this test, follow the steps below:

1. Login on lxplus (new session, please)
2. Generate a grid proxy with `lhcb-proxy-init`
3. A WG production can then be tested using one of the following

```bash
# For productions which don't yet have a step
lb-run LHCbDIRAC/prod ./test_production.py \
  --bkpath='/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20r1/90000000/CHARM.MDST' \
  --app DaVinci --app-version v42r6p1 \
  -o '$HOME/2017-11-18_D2KsH_SerenaMaccolini/fromSerena/DataType2011.py' \
  -o '$HOME/2017-11-18_D2KsH_SerenaMaccolini/fromSerena/D2KS0H_D2PhiPi_Run1.py' \
  --output-types tuple_D2KS0H_D2PhiPi.root

# Once a step is made in the dirac portal the config can be loaded from there instead
lb-run LHCbDIRAC/prod ./test_production.py \
  --bkpath='/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20r1/WGCharmSelection2/90000000/CHARM.MDST' \
  --step-id=132810
```


## New method

 1. `source /cvmfs/lhcb.cern.ch/group_login.sh`
 2. `lhcb-proxy-init`
 3. `cd options/WGProductions/RareCharm/`
 4. Create `test.sh` and `submit.sh`
 5. `lb-run LHCbDIRAC/prod ../../../python/CharmConfig/TestWGProductions/run_many_tests.sh test.sh 1`
    (1 means use production step instead of manually specified options)
 6. `lb-run LHCbDIRAC/prod bash ../../../options/WGProductions/RareCharm/submit.sh`

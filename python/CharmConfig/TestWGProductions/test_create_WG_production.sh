#!/usr/bin/env bash

# ./create_WG_production.py --compare='42540' --print \
#     --name='Charm D2KS0H_D2PhiPi 2011 MagDown' \
#     --author='cburr' --step='132810' --step='132830' --dq-flag='OK' \
#     --inform-email='serena.maccolini@studio.unibo.it' \
#     --bk-query='/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20r1/90000000/CHARM.MDST'

# ./create_WG_production.py --compare='41900' --print \
#     --name='cburr Charm D2hll ntuples - MC 2016 MagUp 23123001' \
#     --author='cburr' --step='132578' --dq-flag='ALL' \
#     --bk-query='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/23123001/ALLSTREAMS.MDST'

./create_WG_production.py --compare='46112' --print \
    --name='Charm D2KS0H_D2PhiPi 2017 MagDown' \
    --author='cburr' --step='133172' --step='132830' --dq-flag='OK' \
    --inform-email='serena.maccolini@studio.unibo.it' \
    --bk-query='/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMCHARGED.MDST'

./create_WG_production.py --compare='46113' --print \
    --name='Charm D2KS0H_D2PhiPi 2017 MagUp' \
    --author='cburr' --step='133172' --step='132830' --dq-flag='OK' \
    --inform-email='serena.maccolini@studio.unibo.it' \
    --bk-query='/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo04/94000000/CHARMCHARGED.MDST'

./create_WG_production.py --compare='46153' --print \
    --name='Charm Ds2PhiPi 2017 MagDown' \
    --author='cburr' --step='133173' --step='132830' --dq-flag='OK' \
    --inform-email='serena.maccolini@studio.unibo.it' \
    --bk-query='/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMSPEC.MDST'

./create_WG_production.py --compare='46154' --print \
    --name='Charm Ds2PhiPi 2017 MagUp' \
    --author='cburr' --step='133173' --step='132830' --dq-flag='OK' \
    --inform-email='serena.maccolini@studio.unibo.it' \
    --bk-query='/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo04/94000000/CHARMSPEC.MDST'
